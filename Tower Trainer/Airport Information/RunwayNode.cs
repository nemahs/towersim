﻿namespace Tower_Trainer.Airport_Information
{
    /// <summary>
    /// Represents the start of a runway
    /// </summary>
    public class RunwayNode : Node
    {
        public double Heading { get; }

        public RunwayNode(double lat, double lon, double heading) : base(lat, lon)
        {
            Heading = heading;
        }
    }
}
