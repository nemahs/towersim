﻿using System;

namespace Tower_Trainer.Airport_Information
{
    /// <summary>
    /// Represents an edge on the airport graph connecting various Taxi and Parking nodes
    /// </summary>
    public class Link
    {
        public enum LinkType { RUNWAY, TAXIWAY, PATH, PARKING, VEHICLE };

        public int Start { get; }
        public int End { get; }
        public int Number { get; }
        public LinkType Type { get; }
        public String Designator {
            get
            {
                return desig;
            }
            private set
            {
                desig = value == "N" ? "" : value;
            }
        }

        private string desig;

        public Link (int s, int e, LinkType t, int number, String desig)
        {
            Start = s;
            End = e;
            Type = t;
            Designator = desig;
            Number = number;
        }
    }
}
