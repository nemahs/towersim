﻿using System;
using System.Device.Location;

namespace Tower_Trainer.Airport_Information
{
    /// <summary>
    /// Provides some useful functions for dealing with waypoings
    /// </summary>
    public static class WaypointHelper
    {
        /// <summary>
        /// Converts a distance in meters to nautical miles
        /// </summary>
        /// <param name="distance">A distance in meters</param>
        /// <returns>The distance in nautical miles</returns>
        public static double MetersToNM(double distance)
        {
            return distance / 1852;
        }

        /// <summary>
        /// Converts a distance in nautical miles to meters
        /// </summary>
        /// <param name="distance">A distance in nautical miles</param>
        /// <returns>The distacne in meters</returns>
        public static double NMToMeters(double distance)
        {
            return distance * 1852;
        }

        /// <summary>
        /// Converts a distance in meters to feet
        /// </summary>
        /// <param name="distance">A distance in meters</param>
        /// <returns>The distance in feet</returns>
        public static double MetersToFeet(double distance)
        {
            return distance * 3.28084;
        }

        /// <summary>
        /// Converts a distance in meet to meters
        /// </summary>
        /// <param name="distance">A distance in feet</param>
        /// <returns>The distance in meters</returns>
        public static double FeetToMeters(double distance)
        {
            return distance / 3.28084;
        }

        /// <summary>
        /// Moves a position <paramref name="distance"/> meters along the <paramref name="direction"/> radial
        /// </summary>
        /// <param name="start">The starting position for the movement</param>
        /// <param name="direction">The radial to move along in degrees (ex. a value of 0 moves the waypoint north)</param>
        /// <param name="distance">The distance along the radial to move in meters</param>
        /// <returns>A GeoCoordinate of the new position</returns>
        public static GeoCoordinate Move(GeoCoordinate start, double direction, double distance)
        {
            var result = new GeoCoordinate();
            double directionRad = direction * Math.PI / 180;
            result.Latitude = (distance * Math.Cos(directionRad) / 111111) + start.Latitude;
            result.Longitude = (distance * Math.Sin(directionRad) / Math.Cos(start.Latitude * Math.PI / 180) / 111111) + start.Longitude;
            return result;
        }

        /// <summary>
        /// Converts a node to a geocoordinate 
        /// </summary>
        /// <param name="waypoint">A Node</param>
        /// <returns>That node as a GeoCoordinate</returns>
        public static GeoCoordinate SimconnectToGeoCoordinate(Airport_Information.Node waypoint)
        {
            return new GeoCoordinate(waypoint.Latitude, waypoint.Longitude);
        }
    }
}