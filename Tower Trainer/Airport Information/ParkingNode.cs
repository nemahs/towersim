﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tower_Trainer.Airport_Information
{
    /// <summary>
    /// Represents a parking spot on the airport
    /// </summary>
    public class ParkingNode : Node
    {
        public double Heading { get; }
        public ParkingType Type { get; }
        public enum ParkingType { GA_SMALL, GA_MEDIUM, GA_LARGE, FUEL, VEHICLE, GATE_SMALL, GATE_MEDIUM, GATE_LARGE }

        public ParkingNode(double lat, double lon, ParkingType type, double heading) : base(lat, lon)
        {
            Heading = heading;
            Type = type;
        }
    }
}
