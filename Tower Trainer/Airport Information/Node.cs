﻿namespace Tower_Trainer.Airport_Information
{
    /// <summary>
    /// Represents a position
    /// Used as the basis for other node types
    /// </summary>
    public abstract class Node
    {
        public double Latitude { get; }
        public double Longitude { get; }

        protected Node (double lat, double lon)
        {
            Latitude = lat;
            Longitude = lon;
        }
    }
}
