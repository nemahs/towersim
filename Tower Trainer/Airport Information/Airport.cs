﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Tower_Trainer.Airport_Information
{
    /// <summary>
    /// Represents an airport in FSX/P3D that we can navigate through
    /// 
    /// An airport is effectively a graph with different node types connected by links
    /// </summary>
    public class Airport
    {
        public double Lat { get; }
        public double Lon { get; }
        public Dictionary<int, TaxiNode> Nodes { get; }
        public Dictionary<string, RunwayNode> RunwayStart { get; }
        public List<Link> Links { get; }
        public Dictionary<int, ParkingNode> ParkingSpots { get; }
        public BiDictionary<int, String> TaxiNames { get; }

        public Airport(double lat, double lon, Dictionary<string, RunwayNode> runway, Dictionary<int, TaxiNode> nodes, List<Link> links,
            Dictionary<int, ParkingNode> parking, BiDictionary<int, String> names)
        {
            Lat = lat;
            Lon = lon;
            RunwayStart = runway;
            Nodes = AddHoldShortInfo(nodes, links);
            Links = links;
            ParkingSpots = parking;
            TaxiNames = names;
        }

        /// <summary>
        /// Tags HOLD_SHORT TaxiNodes with the runways they are short of
        /// </summary>
        /// <param name="nodes">A dictionary of TaxiNodes</param>
        /// <param name="links">A List of links</param>
        /// <returns>A dictionary of TaxiNodes, properly tagged</returns>
        private static Dictionary<int, TaxiNode> AddHoldShortInfo(Dictionary<int, TaxiNode> nodes, List<Link> links)
        {
            foreach (KeyValuePair<int, TaxiNode> node in nodes)
            {
                if (node.Value.Type == TaxiNode.NodeType.HOLD_SHORT)
                {
                    //Find all connecting links
                    var connecting = links.FindAll(x => x.Start == node.Key || x.End == node.Key);
                    var toSearch = new List<Link>();
                    foreach (Link link in connecting)
                    {
                        var nextNode = link.Start == node.Key ? link.End : link.Start;
                        var next_level = links.FindAll(x => x.Start == nextNode || x.End == nextNode);
                        toSearch.AddRange(next_level);
                    }

                    foreach (Link link in toSearch)
                    {
                        if (link.Type == Link.LinkType.RUNWAY)
                        {
                            node.Value.ShortOf = link.Number.ToString() + link.Designator;
                            break;
                        }
                    }
                }
            }
            return nodes;
        }

        /// <summary>
        /// Traverses the airport graph to find the path from <paramref name="position"/> and follows <paramref name="path"/>
        /// </summary>
        /// <param name="path">A comma separated path to follow, ending at a runway (ex. K,9L)</param>
        /// <param name="position">The node to start on</param>
        /// <returns>The path through the airport, if one exists</returns>
        public List<Node> getRoute(String path, Node position)
        {
            //TODO: add sanity checks for the route
            string[] route = path.Split(',');

            return TaxiBFS(route, position);
        }

        /// <summary>
        /// Takes a list of taxiways/runways and returns the nodes to get from the position though the path
        /// </summary>
        /// <param name="taxiWay">A list of taxiways and runways to follow</param>
        /// <param name="position">The start position</param>
        /// <returns>The path through the airport, if one exists</returns>
        private List<Node> TaxiBFS(string[] taxiWay, Node position)
        {
            var route = new List<Node>();
            //Find the closest node to start on
            int startNodeKey = GetClosestNode(position.Latitude, position.Longitude);

            const string runwayPattern = "[0-9]+[A-Z]*";
            var currentTaxiway = "";
            int currentNode = startNodeKey;
            //Build route for each taxiway
            foreach (string dest in taxiWay)
            {
                //Is the dest a taxiway or a runway?
                bool isRunway = Regex.IsMatch(dest, runwayPattern);
                //Runway variables
                RunwayNode runwayNode = null;
                int nearestNode = -1;

                if (isRunway)
                {
                    RunwayStart.TryGetValue(dest, out runwayNode);
                    nearestNode = FindNearestTaxiNode(runwayNode);
                }

                var stack = new Queue<List<int>>();
                var startList = new List<int>
                {
                    currentNode
                };
                stack.Enqueue(startList);

                while (stack.Count > 0)
                {
                    //Get Path from stack
                    var cPath = stack.Dequeue();
                    int cNode = cPath.Last();
                    //Get all links connected to the node in the stack
                    var links = Links.FindAll(x => x.Type != Link.LinkType.PARKING && (x.Start == cNode || x.End == cNode));
                    Link test = null;
                    //Are any links our destination?
                    if (!isRunway)
                    {
                        int number = -1;
                        TaxiNames.TryGetBySecond(dest, out number);
                        test = links.Find(x => x.Number == number);
                    }

                    //if yes
                    if (test != null || (isRunway && cNode == nearestNode))
                    {
                        //Add path to route and break loop
                        foreach (int i in cPath)
                        {
                            Nodes.TryGetValue(i, out TaxiNode n);
                            route.Add(n);
                        }

                        if (isRunway && runwayNode != null)
                        {
                            double distance = .0005;
                            double DegToRad = Math.PI / 180;
                            double newLat = runwayNode.Latitude + (distance * Math.Cos(runwayNode.Heading * DegToRad));
                            double newLong = runwayNode.Longitude + (distance * Math.Sin(runwayNode.Heading * DegToRad));

                            route.Add(new RunwayNode(newLat, newLong, runwayNode.Heading));
                        }

                        currentNode = cPath.Last();
                        break;
                    }
                    else //If no
                    {
                        //See if any links are on our taxiway
                        List<Link> sameTaxiway;
                        int taxiNumber = 0;

                        if (currentTaxiway != "") //No taxiway implies we're at parking
                        {
                            TaxiNames.TryGetBySecond(currentTaxiway, out taxiNumber);
                            sameTaxiway = links.FindAll(x => x.Number == taxiNumber);

                            if (sameTaxiway.Count == 0)
                            {
                                sameTaxiway = links.FindAll(x => x.Number == 0);
                            }
                        }
                        else
                        {
                            sameTaxiway = links;
                        }
                        if (sameTaxiway.Count > 0) //if yes
                        {
                            //Add the other side of the node to the path and add the path to the stack
                            foreach (Link link in sameTaxiway)
                            {
                                var newPath = new List<int>(cPath);
                                if (!cPath.Contains(link.Start))
                                {
                                    newPath.Add(link.Start);
                                    stack.Enqueue(newPath);
                                }
                                if (!cPath.Contains(link.End))
                                {
                                    newPath.Add(link.End);
                                    stack.Enqueue(newPath);
                                }
                            }
                        }
                    }
                }

                currentTaxiway = dest;
            }
            //find node and pathfind to second location
            if (route.Last() is RunwayNode)
            {
                return route;
            }
            return null;
        }

        /// <summary>
        /// Finds the closes TaxiNode on the graph to a lat/lon
        /// </summary>
        /// <param name="lat"></param>
        /// <param name="lon"></param>
        /// <returns></returns>
        public int GetClosestNode(double lat, double lon)
        {
            int closest = -1;
            double closestDistance = 99999;
            foreach (KeyValuePair<int, TaxiNode> el in Nodes)
            {
                Node node = el.Value;
                // find the distance using a^2 + b^2 = c^2
                double distance = Math.Sqrt(Math.Pow((node.Latitude - lat), 2) + Math.Pow((node.Longitude - lon), 2));
                if (distance < closestDistance)
                {
                    closestDistance = distance;
                    closest = el.Key;
                }
            }
            return closest;
        }

        /// <summary>
        /// Finds the nearest taxi node to another node
        /// </summary>
        /// <param name="runwaynode"></param>
        /// <returns></returns>
        private int FindNearestTaxiNode(Node runwaynode)
        {
            return GetClosestNode(runwaynode.Latitude, runwaynode.Longitude);
        }
    }
}