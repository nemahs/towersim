﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tower_Trainer.Airport_Information
{
    /// <summary>
    /// Represents a node on an airport graph
    /// </summary>
    public class TaxiNode : Node
    {
        public enum NodeType { NORMAL, HOLD_SHORT };
        public NodeType Type { get; }
        public string ShortOf { get; set; }

        public bool orientation;
        /// <summary>
        /// Creates a node in the taxi graph
        /// </summary>
        /// <param name="lat">Latitude of the point in decimal notation</param>
        /// <param name="lon">Longitude of the point in decimal notation</param>
        /// <param name="type">Type of the Node</param>
        public TaxiNode (double lat, double lon, NodeType type = NodeType.NORMAL, bool orient = false) : base(lat,lon)
        {
            Type = type;
            orientation = orient;
        }


    }
}
