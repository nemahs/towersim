﻿namespace Tower_Trainer.Airport_Information
{
    /// <summary>
    /// Represents nodes placed in the air
    /// </summary>
    internal class FlightNode : Node
    {
        public double Altitude { get; }

        public FlightNode(double lat, double lon, double alt) : base(lat, lon)
        {
            Altitude = alt;
        }
    }
}