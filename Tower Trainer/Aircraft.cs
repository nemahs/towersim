﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using Tower_Trainer.Airport_Information;
using Tower_Trainer.Directions;
using Tower_Trainer.P3D;

namespace Tower_Trainer
{
    /// <summary>
    /// Base class to represent the simulation aircraft.
    /// 
    /// Currently this class represents VFR departures, but in future should be abstracted to add more flight types
    /// ex. VFR Arrivals, IFR Arrivals, IFR Departures, Pattern Work
    /// </summary>
    public class Aircraft
    {
        public enum AircraftStatus { Spawned = 0, Taxiing, HoldShort, ShortOfRunway, LUAW, Departing, Waiting, InvalidRoute };
        public uint ID { get; }
        public string Name { get; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public int CurrentWaypoint { get; set; }
        public double GroundVelocity { get; set; }
        public double Heading { get; set; }

		private AircraftStatus status;
        public AircraftStatus Status
		{
			get { return status; }

			protected set
			{
				status = value;
				awaitingResponse = false;
				awaitingCycles = 0;
			}
		}
        public Airport CurrentAirport { get; set; }
        private bool holdOrientation;
        private bool heldBefore;
        private bool skipNode;
        private readonly IConnector connector;
        private readonly ISimulation simulation;
        public Direction AircraftDirection { get; }
        public string DestinationRunway { get; set; }
        public string SpeakString { get; set; }
		public string ShortOf { get; private set; }

        // only determines if North/South. Future updates should add more to this
        public string Position { get { return ((Latitude > CurrentAirport.Lat) ? "North Ramp" : "South Ramp");}}

        public int holdNode= -1;
        private Node[] fullRoute;
        private double runwayHeading = -1;
        private int killPoint = -20;

		private bool awaitingResponse = false;
		private int awaitingCycles = 0;
		private const int MAX_CYCLES = 60;

		public event EventHandler ResponseRequested;

        public Aircraft(uint id, String name, ISimulation sim, IConnector connector, Airport cAirport, double lat = 0, double lon = 0)
        {
            this.connector = connector;
            simulation = sim;
            ID = id;
            Name = name;
            Latitude = lat;
            Longitude = lon;
            CurrentAirport = cAirport;
            Status = AircraftStatus.Spawned;
            AircraftDirection = Direction.CreateRandomDirection();
        }

        /// <summary>
        /// Updates the aircraft's information with data from the simulation
        /// Should be called every second or so
        /// NOTE: This function is probably a bit overloaded and sould be split into smaller functions
        /// </summary>
        /// <param name="info">The AircraftInfo struct recieved from the sim</param>
        public void Update(P3D.Connector.AircraftInfo info)
        {
            Latitude = info.lat;
            Longitude = info.lon;
            CurrentWaypoint = info.currentWaypoint;
            GroundVelocity = info.groundVelocity;
            Heading = info.heading * 180 / Math.PI;


			if (awaitingResponse)
			{
				// Count how many update cycles the aircraft has been waiting
				// through
				awaitingCycles++;

				// if it's been waiting for too long, request a response once again
				if (awaitingCycles > MAX_CYCLES)
				{
					OnResponseRequested();
					awaitingCycles = 0;
				}
			}

			else
			{
				if (Status == AircraftStatus.HoldShort)
				{
					var node = fullRoute[holdNode] as TaxiNode;
					ShortOf = node.ShortOf;
				}

				if (Status != AircraftStatus.Taxiing && Status != AircraftStatus.Waiting &&
							  Status != AircraftStatus.Departing)
				{
					OnResponseRequested();
				}

				if (CurrentWaypoint == -1)
				{
					switch (Status)
					{
						case AircraftStatus.Taxiing:
							Status = (fullRoute.Length - 1) - holdNode == 2 ? AircraftStatus.ShortOfRunway : AircraftStatus.HoldShort;
							break;
					}
				}

				if (CurrentWaypoint == killPoint && Status == AircraftStatus.Departing)
				{
					OnResponseRequested();
					connector.DestroyAircraft(this);
				}

				DetectCollisions();
			}
		}

        /// <summary>
        /// Asks whether an aircraft is in front of another aircraft.
        /// Called like a sentence (ex. a.InFrontOf(b) asks "Is a in front of b?")
        /// </summary>
        /// <param name="a">The aircraft that we're seeing if we're in front of</param>
        /// <returns>True if we are in front of a, False otherwise</returns>
        public bool InFrontOf(Aircraft a)
        {
            const int coneSize = 60; //Size of the "InFrontOf" cone

            //Get vector between two aircraft
            var them = new Vector(Longitude, Latitude);
            var us = new Vector(a.Longitude, a.Latitude);
            var diff = them.Subtract(us);
            //Is heading between +/- coneSize of a's heading?
            return Math.Abs(diff.Heading - a.Heading) < coneSize;
        }

        /// <summary>
        /// Detects if an aircraft is in front of us that we're going to hit.
        /// If there is, stop us
        /// If there isn't and we are waiting, have us go again
        /// </summary>
        private void DetectCollisions()
        {
            const int collisionDistance = 70; //Detection distance for us to stop

            var aircraftInTheWay = false;
            foreach (Aircraft a in simulation.CurrentAircraft)
            {
                var us = new GeoCoordinate(Latitude, Longitude);
                var them = new GeoCoordinate(a.Latitude, a.Longitude);

                if (a != this && a.Status != AircraftStatus.Spawned && us.GetDistanceTo(them) < collisionDistance && a.InFrontOf(this))
                {
                    aircraftInTheWay = true;
                    if (Status == AircraftStatus.Taxiing)
                    {
                        Console.WriteLine(a.Name + "is in front of me, stopping");
                        //Hold position
                        Stop();
                        Status = AircraftStatus.Waiting;
                    }

                    break;
                }
            }

            if (!aircraftInTheWay && Status == AircraftStatus.Waiting)
            {
                Go();
                Status = AircraftStatus.Taxiing;
            }
        }

        /// <summary>
        /// Converts a route given in string form to its Node array equivalent
        /// </summary>
        /// <param name="route">A comma separated route to convert (ex. K,9L)</param>
        /// <returns>A Node array of nodes for the aircaft to follow to taverse the route</returns>
        private Node[] ParseRoute(String route)
        {
            var info = new TaxiNode(Latitude, Longitude);
            var parsedRoute = simulation.CurrentAirport.getRoute(route, info);
            if (parsedRoute != null)
            {
                //If we have a runway, set the runwayHeading to its heading (NOTE: This is probably wrong)
                for (int i = 0; i < parsedRoute.Count; i++)
                {
                    if (parsedRoute[i] is RunwayNode)
                    {
                        runwayHeading = ((RunwayNode)parsedRoute[i]).Heading;
                    }
                }

                holdNode = -1;
                return parsedRoute.ToArray();
            }
            return null;
        }

        /// <summary>
        /// Sets the route on the aircraft and starts the aircraft moving if the route is valid
        /// </summary>
        /// <param name="stringRoute">A comma separated route for the aircraft to follow(ex. K,9L)</param>
        /// <returns>True if the route was valid and the aircraft is following it, False otherwise</returns>
        public bool SetRoute(string stringRoute)
        {

            if (Status == AircraftStatus.Spawned || Status == AircraftStatus.InvalidRoute)
            {
                var nodes = stringRoute.Split(',');
                DestinationRunway = nodes[nodes.Length - 1];

                fullRoute = ParseRoute(stringRoute);

                //Update in sim
                if (fullRoute != null)
                {
					Continue();
                    Status = AircraftStatus.Taxiing;
                    return true;
                }
                else
                {
                    Status = AircraftStatus.InvalidRoute;
                }
            }

            return false;
        }

        /// <summary>
        /// Tells the aircraft to takeoff
        /// 
        /// The aircraft won't takeoff unless its holding short of the runway or lined up on the runway
        /// </summary>
        /// <param name="runway">The runway the aircraft should takeoff from</param>
        /// <returns>True if the aircraft can and is departing from <paramref name="runway"/>, False otherwise</returns>
        public bool Depart(string runway)
        {
            if (Status == AircraftStatus.LUAW || Status == AircraftStatus.ShortOfRunway &&
				runway.Trim().Equals(DestinationRunway))
            {
                var waypoints = new List<Node>();
                waypoints.AddRange(new List<Node>(fullRoute).GetRange(holdNode, fullRoute.Length - holdNode));

                //First waypoint
                var climbPos = WaypointHelper.Move(WaypointHelper.SimconnectToGeoCoordinate(fullRoute[fullRoute.Length - 1]), runwayHeading, 3500);
                waypoints.Add(new FlightNode(climbPos.Latitude, climbPos.Longitude, 1000));

                //End waypoint
                var pos = WaypointHelper.Move(new GeoCoordinate(CurrentAirport.Lat, CurrentAirport.Lon), AircraftDirection.Heading, WaypointHelper.NMToMeters(5));
                waypoints.Add(new FlightNode(pos.Latitude, pos.Longitude, 3000));

                Status = AircraftStatus.Departing;
                killPoint = waypoints.Count;
                connector.MoveAircraft(this, waypoints.ToArray());
                return true;
            }

            return false;
        }

        /// <summary>
        /// Checks to see if we should hold at this node or not
        /// NOTE: This function has side-effects and probably shouldn't
        /// </summary>
        /// <param name="node">The node to see if we should hold at</param>
        /// <returns>True if we should hold here, False otherwise</returns>
        private bool StopAtHold(TaxiNode node)
        {
            if (heldBefore)
            {
                if (node.orientation == holdOrientation && !skipNode)
                {
                    skipNode = true;
                    return false;
                }
            }
            holdOrientation = !node.orientation;
            heldBefore = true;
            return true;
        }

        /// <summary>
        /// Stops the aircraft whatever it's doing (sets its sim speed to 0)
        /// </summary>
        public void Stop()
        {
            connector.Stop(this);
        }

        /// <summary>
        /// Tells the aircraft to continue whatever it was doing (sets its sim speed to taxiSpeed
        /// </summary>
        public void Go()
        {
            const int taxiSpeed = 20;

            connector.SetSpeed(this, taxiSpeed);
        }

        /// <summary>
        /// Moves the aircraft along its route if it isn't already.
        /// 
        /// Used in three cases:
        /// - To start the aircraft moving when it gets a route
        /// - To move the aircraft along the route after holding short
        /// - To have the aircraft line up and wait
        /// </summary>
        private void Continue()
        {
            if (CurrentWaypoint == -1)
            {
                //Generate a new route and move us
                var newRoute = new List<Node>();
                holdNode++;
                while (holdNode < fullRoute.Length)
                {
                    var node = fullRoute[holdNode];
                    newRoute.Add(node);
                    if (node is TaxiNode checkNode && checkNode.Type == TaxiNode.NodeType.HOLD_SHORT && StopAtHold(checkNode))
                    {
                        break;
                    }
                    holdNode++;
                }
                connector.MoveAircraft(this, newRoute.ToArray());
            }
        }

        /// <summary>
        /// Tells the aircraft to cross <paramref name="runway"/>
        /// </summary>
        /// <param name="runway">The runway name to cross</param>
        /// <returns>True is the aircraft can and is crossing <paramref name="runway"/>, False otherwise</returns>
        public bool ContinueTaxi(string runway)
        {
            if (Status == AircraftStatus.HoldShort &&
				runway.Trim() == ShortOf)
            {
                Continue();
                Status = AircraftStatus.Taxiing;
                return true;
            }

            return false;
        }

        /// <summary>
        /// Tells the aircraft to line up and wait on <paramref name="runway"/>
        /// </summary>
        /// <param name="runway">The runway to line up on</param>
        /// <returns>True if the aircraft can and is lining up on <paramref name="runway"/>, False otherwise</returns>
        public bool LineUpAndWait(string runway)
        {
            if (Status == AircraftStatus.ShortOfRunway &&
				runway.Trim().Equals(DestinationRunway))
            {
                Continue();
                Status = AircraftStatus.LUAW;
                return true;
            }

            return false;
        }

        /// <summary>
        /// Called when we need to wait for a response from the user
        /// </summary>
        protected virtual void OnResponseRequested()
        {
            awaitingResponse = true;
			ResponseRequested?.Invoke(this, EventArgs.Empty);
        }
    }
}