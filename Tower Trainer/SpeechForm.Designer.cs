﻿namespace Tower_Trainer
{
     partial class SpeechForm
     {
          /// <summary>
          /// Required designer variable.
          /// </summary>
          private System.ComponentModel.IContainer components = null;

          /// <summary>
          /// Clean up any resources being used.
          /// </summary>
          /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
          protected override void Dispose(bool disposing)
          {
               if (disposing && (components != null))
               {
                    components.Dispose();
               }
               base.Dispose(disposing);
          }

          #region Windows Form Designer generated code

          /// <summary>
          /// Required method for Designer support - do not modify
          /// the contents of this method with the code editor.
          /// </summary>
          private void InitializeComponent()
          {
               this.output_box = new System.Windows.Forms.RichTextBox();
               this.start_btn = new System.Windows.Forms.Button();
               this.end_btn = new System.Windows.Forms.Button();
               this.SuspendLayout();
               // 
               // output_box
               // 
               this.output_box.Location = new System.Drawing.Point(53, 44);
               this.output_box.Name = "output_box";
               this.output_box.Size = new System.Drawing.Size(165, 139);
               this.output_box.TabIndex = 0;
               this.output_box.Text = "";
               // 
               // start_btn
               // 
               this.start_btn.Location = new System.Drawing.Point(53, 210);
               this.start_btn.Name = "start_btn";
               this.start_btn.Size = new System.Drawing.Size(75, 23);
               this.start_btn.TabIndex = 1;
               this.start_btn.Text = "start";
               this.start_btn.UseVisualStyleBackColor = true;
               this.start_btn.Click += new System.EventHandler(this.start_btn_Click);
               // 
               // end_btn
               // 
               this.end_btn.Location = new System.Drawing.Point(145, 210);
               this.end_btn.Name = "end_btn";
               this.end_btn.Size = new System.Drawing.Size(75, 23);
               this.end_btn.TabIndex = 2;
               this.end_btn.Text = "end";
               this.end_btn.UseVisualStyleBackColor = true;
               this.end_btn.Click += new System.EventHandler(this.end_btn_Click);
               // 
               // SpeechForm
               // 
               this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
               this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
               this.ClientSize = new System.Drawing.Size(284, 262);
               this.Controls.Add(this.end_btn);
               this.Controls.Add(this.start_btn);
               this.Controls.Add(this.output_box);
               this.Name = "SpeechForm";
               this.Text = "SpeechForm";
               this.Load += new System.EventHandler(this.SpeechForm_Load);
               this.ResumeLayout(false);

          }

          #endregion

          private System.Windows.Forms.RichTextBox output_box;
          private System.Windows.Forms.Button start_btn;
          private System.Windows.Forms.Button end_btn;
     }
}