﻿using System;

namespace Tower_Trainer.Directions
{
    /// <summary>
    /// Represents a cardinal direction.
    /// Contains both the heading and string name of the direction
    /// </summary>
    public abstract class Direction
    {
        public int Heading { get; protected set; }
        public string Name { get; protected set; }

        /// <summary>
        /// Creates a random direction and returns it
        /// </summary>
        /// <returns>A randonly selected direction</returns>
        public static Direction CreateRandomDirection()
        {
            var random = new Random();
            switch(random.Next(7))
            {
                case 0:
                    return new North();
                case 1:
                    return new NorthEast();
                case 2:
                    return new East();
                case 3:
                    return new SouthEast();
                case 4:
                    return new South();
                case 5:
                    return new SouthWest();
                case 6:
                    return new West();
                case 7:
                    return new NorthWest();
            }

            throw new Exception();
        }
    }

    public sealed class North: Direction
    {
        public North()
        {
            Heading = 0;
            Name = "north";
        }

    }

    public sealed class NorthEast: Direction
    {
        public NorthEast()
        {
            Heading = 45;
            Name = "north east";
        }
    }

    public sealed class East: Direction
    {
        public East()
        {
            Heading = 90;
            Name = "east";
        }
    }

    public sealed class SouthEast: Direction
    {
        public SouthEast()
        {
            Heading = 135;
            Name = "south east";
        }
    }

    public sealed class South: Direction
    {
        public South()
        {
            Heading = 180;
            Name = "south";
        }
    }

    public sealed class SouthWest: Direction
    {
        public SouthWest()
        {
            Heading = 235;
            Name = "south west";
        }
    }

    public sealed class West: Direction
    {
        public West()
        {
            Heading = 270;
            Name = "west";
        }
    }

    public sealed class NorthWest: Direction
    {
        public NorthWest()
        {
            Heading = 315;
            Name = "north west";
        }
    }
}