﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tower_Trainer.Airport_Information;
using Tower_Trainer.P3D;
using Tower_Trainer.Speech;
using Tower_Trainer.Speech.AircraftMessages;
using Tower_Trainer.VR;

namespace Tower_Trainer
{
    /// <summary>
    /// Represents the state of the current simulation
    /// </summary>
    public class Simulation : ISimulation
    {
        public Airport CurrentAirport { get; }
        public List<Aircraft> CurrentAircraft { get; }
        private readonly IServiceSR<OutputRequestedEventArgs> serviceSR;
        private readonly IServiceTTS<SpeakFinishedEventArgs> serviceTTS;
        private readonly MainForm form;
        public readonly IConnector connector;
        private bool isRunning;
        private int aircraftPerHr;
        private List<string> flightType;
        private readonly IController<TriggerHoldEventArgs> controller;
        private Stack<string> lastAircraftToSpeak;

        public Simulation(MainForm form)
        {
            CurrentAirport = ReadXML.collectData();
            CurrentAircraft = new List<Aircraft>();
            serviceSR = new ServiceSR();
            serviceTTS = new ServiceTTS();
            controller = new XboxController();
            lastAircraftToSpeak = new Stack<string>();
            this.connector = new Connector(this);
            this.form = form;

            controller.TriggerHold += Controller_TriggerHold;
            serviceSR.OutputRequest += ServiceSR_OutputRequest;
			serviceTTS.SpeakFinished += ServiceTTS_SpeakFinished;
        }

		private void ServiceTTS_SpeakFinished(object sender, SpeakFinishedEventArgs e)
		{
			var aircraft = CurrentAircraft.Find(ByName(e.Callsign));
			
			if (aircraft != null)
			{
				lastAircraftToSpeak.Push(e.Callsign);
				aircraft.SpeakString = e.SpeakString;
			}
		}

        private void Controller_TriggerHold(object sender, TriggerHoldEventArgs e)
        {
            if (e.TrigHeld)
            {
                MainForm.Instance.buttonPTT.BackColor = System.Drawing.Color.LimeGreen;
                serviceSR.RecognizeAsync();
                serviceTTS.Pause();
            }
            else
            {
                MainForm.Instance.buttonPTT.BackColor = System.Drawing.Color.Red;
                serviceSR.RecognizeAsyncStop();
                serviceTTS.Resume();
            }
        }

        /// <summary>
        /// Adds an aircraft to the list of current aircraft and queues its spawn-in call
        /// </summary>
        /// <param name="aircraft">The new aircraft being added to the sim</param>
        public void AddAircraft(Aircraft aircraft)
        {
            CurrentAircraft.Add(aircraft);
			aircraft.ResponseRequested += Aircraft_ResponseRequested;
        }

        private void Aircraft_ResponseRequested(object sender, EventArgs e)
        {
      		if (sender is Aircraft aircraft)
			{
                // calls the appropriate message based on the aircraft status
                var message = new AircraftRequest<SpeakFinishedEventArgs>(serviceTTS, aircraft);
                message.Execute();
			}
        }

        /// <summary>
        /// Used to find aircraft by name
        /// </summary>
        /// <param name="callsign">The callsign to compare to</param>
        private Predicate<Aircraft> ByName(string callsign)
		{
			return aircraft => aircraft.Name == callsign;
        }

		private Aircraft FindLastAircraftToSpeak()
		{
			Aircraft lastAircraft = null;
			
			// Make sure that the last aircraft to speak still exists within the simulation
			while (CurrentAircraft.Count != 0
				&& (lastAircraft = CurrentAircraft.Find(ByName(lastAircraftToSpeak.Peek()))) == null)
			{
				lastAircraftToSpeak.Pop();
			}

			return lastAircraft;
		}

        private void ServiceSR_OutputRequest(object sender, OutputRequestedEventArgs e)
        {
            form.ChangeText(e.OutputString);

			e.Context.TryGetValue("callsign", out string callsign);
			e.Context.TryGetValue("readback", out string readback);

			var currentAircraft = CurrentAircraft.Find(ByName(callsign));
			AircraftMessage<SpeakFinishedEventArgs> message = new NoMessage<SpeakFinishedEventArgs>();
			
            if (e.GrammarReferenced == Grammars.VFR_Repeat)
            {
                Aircraft lastAircraft = FindLastAircraftToSpeak();

				// If a callsign is provided in the repeat instruction, have that plane repeat itself
				// Else, use the last aircraft to speak
                var aircraft = currentAircraft ?? lastAircraft;

				message = new RepeatMessage<SpeakFinishedEventArgs>(serviceTTS, aircraft);
            }
            
            // If the ATC said something incomprehensible to an existing aircraft, have the aircraft
			// ask for a repeat
            else if (e.GrammarReferenced == Grammars.General && currentAircraft != null)
            {
				message = new RepeatRequest<SpeakFinishedEventArgs>(serviceTTS, currentAircraft);
            }

			// If the ATC did not mention an existing aircraft, have the last aircraft to speak ask for clarification,
			// or if no aircraft exist in the simulation (each aircraft must at least have reported its spawning),
			// then do nothing
            else if (currentAircraft == null && lastAircraftToSpeak.Count > 0) 
            {
				currentAircraft = FindLastAircraftToSpeak();
				message = new LastAircraftRepeatRequest<SpeakFinishedEventArgs>(serviceTTS, currentAircraft);
            }

			// We only want to give the aircraft actual instructions if the aircraft exists
			else if (currentAircraft != null)
            {
				// keeps track of whether we have a valid readback (i.e. does the instruction
				// make sense in the context of its current status?)
				bool isValid = true;
				
				// check if the instruction is valid (does not have any missing properties even though
				// it was recognized as belonging to a grammar
				if (e.IsValid)
				{
					switch (e.GrammarReferenced)
					{
						case Grammars.VFR_Departure_Initial:
							goto case Grammars.VFR_Departure;

						case Grammars.VFR_Departure:
                            var route = e.Context["taxiway"] + e.Context["runway"];
                            isValid = currentAircraft.SetRoute(route);
                            break;

						case Grammars.VFR_HoldShort_Cross:
							e.Context.TryGetValue("runway", out string runway);
                            isValid = currentAircraft.ContinueTaxi(runway);
                            break;

						case Grammars.VFR_LUAW:
							e.Context.TryGetValue("runway", out runway);
							isValid = currentAircraft.LineUpAndWait(runway);
							break;

						case Grammars.VFR_Takeoff:
							e.Context.TryGetValue("runway", out runway);
							isValid = currentAircraft.Depart(runway);
							break;
					}
				}

                if (!isValid)
                {
                    message = new UnableMessage<SpeakFinishedEventArgs>
                        (serviceTTS, currentAircraft);
                }

				else
				{
					// The readback could contain a request for the ATC to provide some additional information
					message = new Readback<SpeakFinishedEventArgs>(serviceTTS, currentAircraft, e.Context);
				}

			}

			message.Execute();
        }

        /// <summary>
        /// Randomly creates aircraft while the sim is running
        /// </summary>
        private async void AircraftLoopAsync()
        {
            var averageSpawnInSeconds = 3600 / aircraftPerHr;
            const int variance = 60; //in seconds
            var rng = new Random();

            while (isRunning)
            {
                var nextDelay = rng.Next(averageSpawnInSeconds - variance, averageSpawnInSeconds + variance);
                GenerateAircraft();
                await Task.Delay(Math.Abs(nextDelay * 1000));
            }
        }

        /// <summary>
        /// Creates an aircraft with random values and tells simconnect to make it
        /// </summary>
        private void GenerateAircraft()
        {
            //Appropriately randomly decide the type/name/flight type of aircraft
            const string aircraftType = "Mooney Bravo";
            var aircraftName = GenerateAircraftName();
            var rng = new Random();

            var validTypes = new List<ParkingNode.ParkingType> { ParkingNode.ParkingType.GA_LARGE, ParkingNode.ParkingType.GA_MEDIUM, ParkingNode.ParkingType.GA_SMALL };
            int parkingSpot;
            do
            {
                parkingSpot = rng.Next(CurrentAirport.ParkingSpots.Count - 1) % 10 + 1;
            } while (!validTypes.Contains(CurrentAirport.ParkingSpots[parkingSpot].Type));

            //Tell P3D to make it
            connector.CreateAircraft(aircraftType, aircraftName, CurrentAirport.ParkingSpots[parkingSpot]);
        }

        /// <summary>
        /// Randomly generates a valid aircraft callsign
        /// </summary>
        /// <returns>A callsign (ex. N123AB)</returns>
        private string GenerateAircraftName()
        {
            var rng = new Random();
            var validchars = "ABCK".ToCharArray(); //DEFGHIJKLMNOPQRSTUVWXYZ
            var aircraftName = new StringBuilder();
            aircraftName.Append("N");
            var j = rng.Next(2);

            for (int i = 0; i < 4 - j; i++)
            {
                aircraftName.Append(rng.Next(10));
            }
            for (int i = 0; i <= j; i++)
            {
                aircraftName.Append(validchars[rng.Next(validchars.Length - 1)]);
            }
            return aircraftName.ToString();
        }

        /// <summary>
        /// Starts the simulation
        /// </summary>
        public void Start()
        {
            if (!connector.IsConnected)
            {
                throw new Exception("Not connected");
            }
            aircraftPerHr = form.AircraftPerHour;
            flightType = form.FlightType;
            lastAircraftToSpeak = new Stack<string>();
            isRunning = true;
            AircraftLoopAsync();
        }

        /// <summary>
        /// Stops the simulation and cleans up the sim
        /// </summary>
        public void Stop()
        {
            // stop the simulation here
            isRunning = false;
            for (int i = 0; i < CurrentAircraft.Count; i++)
            {
                connector.DestroyAircraft(CurrentAircraft[0]);
            }

			// Reset all SR and TTS, clear all scheduled speech
			serviceSR.RecognizeAsyncStop();
			serviceTTS.Reset();
			lastAircraftToSpeak.Clear();
        }
    }
}