﻿#if (FSX)
    using Microsoft.FlightSimulator.SimConnect;
#else
    using LockheedMartin.Prepar3D.SimConnect;
#endif
using System;
using System.Collections.Generic;
using Tower_Trainer.Airport_Information;

namespace Tower_Trainer.P3D
{
    /// <summary>
    /// The interface between P3D/FSX and the program
    /// 
    /// This file contains the external interface that the rest of the program uses to tell the sim to do something
    /// </summary>
    ///
    public partial class Connector : IConnector
    {
        private readonly Queue<Aircraft> tentativeAircraft = new Queue<Aircraft>();

        /// <summary>
        /// Creates an aircraft inside the sim
        /// </summary>
        /// <param name="type">The aircraft type to make (ex. Mooney Bravo)</param>
        /// <param name="tailNumber">A tail number for the aircraft (ex. N123AB)</param>
        /// <param name="pos">Where the aircraft should be spawned</param>
        private void CreateAircraft(String type, String tailNumber, SIMCONNECT_DATA_INITPOSITION pos)
        {
            simConnect.AICreateNonATCAircraft(type, tailNumber, pos, DATA_REQUESTS.NEW_AIRCRAFT);
            tentativeAircraft.Enqueue(new Aircraft(0, tailNumber, simulation, this, simulation.CurrentAirport, pos.Latitude, pos.Longitude));
        }
        
        /// <summary>
        /// Creates an aircraft in the sim
        /// </summary>
        /// <param name="type">The aircraft type to make (ex. Mooney Bravo)</param>
        /// <param name="tailNumber">A tail number for the aircraft (ex. N123AB)</param>
        /// <param name="position">The Node where the aircraft should spawn</param>
        public void CreateAircraft(String type, String tailNumber, Node position)
        {
            var pos = new SIMCONNECT_DATA_INITPOSITION
            {
                Latitude = position.Latitude,
                Longitude = position.Longitude
            };
            CreateAircraft(type, tailNumber, pos);
        }

        /// <summary>
        /// Creates an aircraft in the sim
        /// </summary>
        /// <param name="aircraftType">The aircraft type to make (ex. Mooney Bravo)</param>
        /// <param name="aircraftName">>A tail number for the aircraft (ex. N123AB)</param>
        /// <param name="parkingSpot">The parking node where the aircraft should spawn</param>
        public void CreateAircraft(string aircraftType, string aircraftName, ParkingNode parkingSpot)
        {
            var position = new SIMCONNECT_DATA_INITPOSITION()
            {
                Latitude = parkingSpot.Latitude,
                Longitude = parkingSpot.Longitude,
                Heading = parkingSpot.Heading + 180,
                OnGround = 1,
                Airspeed = 0
            };

            if (position.Heading > 360)
                position.Heading = position.Heading - 360;

            CreateAircraft(aircraftType, aircraftName, position);
        }

        /// <summary>
        /// Removes an aircraft from the sim
        /// </summary>
        /// <param name="aircraft">The aircraft to be deleted</param>
        public void DestroyAircraft(Aircraft aircraft)
        {
			simConnect.RequestDataOnSimObject((DATA_REQUESTS)(200 + aircraft.ID), DEFINITIONS.AircraftInfo, aircraft.ID, SIMCONNECT_PERIOD.NEVER, 0, 0, 0, 0);
            simConnect.AIRemoveObject(aircraft.ID, DATA_REQUESTS.KILL_AIRCRAFT);
            //This probably needs to me moved
            simulation.CurrentAircraft.Remove(aircraft);
        }

        /// <summary>
        /// Requests info about the user
        /// </summary>
        public void RequestPlayerInfo()
        {
            simConnect.RequestDataOnSimObjectType(DATA_REQUESTS.GET_PLAYERINFO, DEFINITIONS.AircraftInfo, 0, SIMCONNECT_SIMOBJECT_TYPE.USER);
        }

        /// <summary>
        /// Requests a list of the nearest airports to the user (bugged)
        /// </summary>
        public void RequestAirportList()
        {
            simConnect.RequestFacilitiesList(SIMCONNECT_FACILITY_LIST_TYPE.AIRPORT, DATA_REQUESTS.GET_AIRPORTLIST);
        }

        /// <summary>
        /// Tells an aircraft to move
        /// </summary>
        /// <param name="aircraft">The aircraft to move</param>
        /// <param name="waypoints">A list of waypoints for the aircraft to follow</param>
        public void MoveAircraft(Aircraft aircraft, Node[] waypoints)
        {
            var simPoints = new SIMCONNECT_DATA_WAYPOINT[waypoints.Length];

            for (int i = 0; i < waypoints.Length; i++)
            {
                simPoints[i] = ConvertToSimConnect(waypoints[i]);
            }

            //Issue command
            var obj = new Object[waypoints.Length];
            simPoints.CopyTo(obj, 0);
            simConnect.SetDataOnSimObject(DEFINITIONS.TaxiWaypoints, aircraft.ID, 0, obj);
        }

        /// <summary>
        /// Sets the taxi speed of the aircraft in the sim
        /// </summary>
        /// <param name="aircraft">The aircraft to set the speed on</param>
        /// <param name="speed">What the taxi speed should be set to</param>
        public void SetSpeed(Aircraft aircraft, int speed)
        {
            simConnect.SetDataOnSimObject(DEFINITIONS.Speed, aircraft.ID, 0, 1);
            simConnect.SetDataOnSimObject(DEFINITIONS.CruiseSpeed, aircraft.ID, 0, speed);
        }

        /// <summary>
        /// Tells an aircraft to stop taxiing
        /// </summary>
        /// <param name="aircraft">The aircraft to stop</param>
        public void Stop(Aircraft aircraft)
        {
            SetSpeed(aircraft, 0);
        }
    }
}