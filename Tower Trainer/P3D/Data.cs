﻿#if (FSX)
    using Microsoft.FlightSimulator.SimConnect;
#else
    using LockheedMartin.Prepar3D.SimConnect;
#endif
using System;
using System.Runtime.InteropServices;

namespace Tower_Trainer.P3D
{
    /// <summary>
    /// The interface between FSX/P3D and the program
    /// 
    /// This file contains the data structures used to communicate between the program and the sim
    /// It is also responsible for telling the sim about these structures on startup
    /// </summary>
    public partial class Connector
    {
        public const int WM_USER_SIMCONNECT = 0x0402;

        //Needed enums/data structs

        /// <summary>
        /// IDs to reference custom structs
        /// </summary>
        public enum DEFINITIONS
        {
            AircraftInfo,
            TaxiWaypoints,
            CurrentWaypoint,
            Speed,
            CruiseSpeed,
        };

        /// <summary>
        /// IDs for any requests to/from the sim
        /// </summary>
        public enum DATA_REQUESTS
        {
            GET_PLAYERINFO,
            GET_AIRPORTLIST,
            NEW_AIRCRAFT,
            GET_AIRCRAFTINFO,
            KILL_AIRCRAFT,
        };

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]
        public struct AircraftInfo
        {
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
            public String title;

            public double lat;
            public double lon;
            public double altitude;
            public int currentWaypoint;
            public double groundVelocity;
            public double heading;
        }

        public static void SetupStructs(SimConnect sim)
        {
            SetupInfo(sim);
        }

        /// <summary>
        /// Sets up all the data structures by telling the sim about them
        /// </summary>
        /// <param name="simconnect"></param>
        private static void SetupInfo(SimConnect simconnect)
        {
            simconnect.AddToDataDefinition(DEFINITIONS.AircraftInfo, "title", null, SIMCONNECT_DATATYPE.STRING256, 0.0f, SimConnect.SIMCONNECT_UNUSED);
            simconnect.AddToDataDefinition(DEFINITIONS.AircraftInfo, "Plane Latitude", "degrees", SIMCONNECT_DATATYPE.FLOAT64, 0.0f, SimConnect.SIMCONNECT_UNUSED);
            simconnect.AddToDataDefinition(DEFINITIONS.AircraftInfo, "Plane Longitude", "degrees", SIMCONNECT_DATATYPE.FLOAT64, 0.0f, SimConnect.SIMCONNECT_UNUSED);
            simconnect.AddToDataDefinition(DEFINITIONS.AircraftInfo, "Plane Altitude", "feet", SIMCONNECT_DATATYPE.FLOAT64, 0.0f, SimConnect.SIMCONNECT_UNUSED);
            simconnect.AddToDataDefinition(DEFINITIONS.AircraftInfo, "AI CURRENT WAYPOINT", "Number", SIMCONNECT_DATATYPE.INT32, 0.0f, SimConnect.SIMCONNECT_UNUSED);
            simconnect.AddToDataDefinition(DEFINITIONS.AircraftInfo, "GROUND VELOCITY", "Knots", SIMCONNECT_DATATYPE.FLOAT64, 0.0f, SimConnect.SIMCONNECT_UNUSED);
            simconnect.AddToDataDefinition(DEFINITIONS.AircraftInfo, "PLANE HEADING DEGREES TRUE", "Radians", SIMCONNECT_DATATYPE.FLOAT64, 0.0f, SimConnect.SIMCONNECT_UNUSED);
            simconnect.AddToDataDefinition(DEFINITIONS.TaxiWaypoints, "AI WAYPOINT LIST", null, SIMCONNECT_DATATYPE.WAYPOINT, 0.0f, SimConnect.SIMCONNECT_UNUSED);
            simconnect.AddToDataDefinition(DEFINITIONS.Speed, "AI GROUNDTURNSPEED", "Knots", SIMCONNECT_DATATYPE.INT32, 0.0f, SimConnect.SIMCONNECT_UNUSED);
            simconnect.AddToDataDefinition(DEFINITIONS.CruiseSpeed, "AI GROUNDCRUISESPEED", "Knots", SIMCONNECT_DATATYPE.INT32, 0.0f, SimConnect.SIMCONNECT_UNUSED);
            simconnect.RegisterDataDefineStruct<AircraftInfo>(DEFINITIONS.AircraftInfo);
        }
    }
}