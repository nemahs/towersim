﻿using System;
using Microsoft.FlightSimulator.SimConnect;
using Tower_Trainer.Airport_Information;

namespace Tower_Trainer.P3D
{
    /// <summary>
    /// Provides an interface for interacting with the sim/mock
    /// </summary>
    public interface IConnector
    {
        bool IsConnected { get; }

        void CloseConnection();
        void CreateAircraft(string aircraftType, string aircraftName, ParkingNode parkingSpot);
        void CreateAircraft(string type, string tailNumber, Node position);
        void DestroyAircraft(Aircraft aircraft);
        void MoveAircraft(Aircraft aircraft, Node[] waypoints);
        void OpenConnection(IntPtr handle);
        void ReceiveMessage();
        void RequestAirportList();
        void RequestPlayerInfo();
        void SetSpeed(Aircraft aircraft, int speed);
        void Stop(Aircraft aircraft);
    }
}