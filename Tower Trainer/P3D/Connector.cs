﻿#if (FSX)
    using Microsoft.FlightSimulator.SimConnect;
#else
    using LockheedMartin.Prepar3D.SimConnect;
#endif
using System;
using System.Runtime.InteropServices;
using Tower_Trainer.Airport_Information;

namespace Tower_Trainer.P3D
{
    /// <summary>
    ///  The interface between P3D/FSX and the program
    ///  
    /// This file contains the core connection components of simconnect
    /// and handles initialization and event handling from the sim
    /// </summary>
    public partial class Connector
    {
        private SimConnect simConnect; 
        public bool IsConnected { get; private set; }
        private readonly ISimulation simulation;
        private const int aircraftStartingID = 200;

        public Connector(ISimulation simulation)
        {
            this.simulation = simulation;
        }

        /// <summary> Opens a SimConnect connection to P3D/FSX </summary>
        /// <param name="handle"> A handle to the process window handling events</param>
        public void OpenConnection(IntPtr handle)
        {
            if (simConnect == null)
            {
                try
                {
                    simConnect = new SimConnect("Tower Trainer", handle, WM_USER_SIMCONNECT, null, 0);
                    InitDataRequest();
                }
                catch (COMException e)
                {
                    //Console.WriteLine(e.Message);
                }
            }
        }

        /// <summary>
        /// Closes the connection to P3D/FSX
        /// </summary>
        public void CloseConnection()
        {
            if (simConnect != null)
            {
                simConnect.Dispose();
                simConnect = null;
                IsConnected = false;
            }
        }

        /// <summary>
        /// Receives any waiting SimConnect messages
        /// </summary>
        public void ReceiveMessage()
        {
            if (simConnect != null)
            {
                simConnect.ReceiveMessage();
            }
        }

        /// <summary>
        /// Sets up any initial handlers and data structures needed
        /// </summary>
        private void InitDataRequest()
        {
            try
            {
                simConnect.OnRecvOpen += SimConnect_OnRecvOpen;
                simConnect.OnRecvQuit += SimConnect_OnRecvQuit;
                simConnect.OnRecvSimobjectDataBytype += GetData;
                simConnect.OnRecvSimobjectData += GetData;
                simConnect.OnRecvAssignedObjectId += SimConnect_OnRecvAssignedObjectId;
                simConnect.OnRecvException += SimConnect_OnRecvException;
                simConnect.OnRecvAirportList += SimConnect_OnRecvAirportList;

                //

                SetupStructs(simConnect);
            }
            catch (COMException e)
            {
                throw;
            }
        }

        /// <summary>
        /// Processes data recieved from the sim as a result of a request for information
        /// </summary>
        /// <param name="conn">The open simconnect connection</param>
        /// <param name="data">The data structure recieved</param>
        private static void GetData(SimConnect conn, SIMCONNECT_RECV_SIMOBJECT_DATA_BYTYPE data)
        {
            switch ((DATA_REQUESTS)data.dwRequestID)
            {
                case DATA_REQUESTS.GET_PLAYERINFO:
                    var info = (AircraftInfo)data.dwData[0];
                    Console.WriteLine("Title: " + info.title);
                    Console.WriteLine("Lat:" + info.lat);
                    Console.WriteLine("Lon: " + info.lon);
                    break;
            }
        }

        /// <summary>
        /// Processes data recieved as a result of a request for information
        /// </summary>
        /// <param name="conn">The open simconnect connection</param>
        /// <param name="data">The data structure recieved from the game</param>
        private void GetData(SimConnect conn, SIMCONNECT_RECV_SIMOBJECT_DATA data)
        {
            if (data.dwRequestID > aircraftStartingID)
            {
                var aircraft = simulation.CurrentAircraft.Find(x => x.ID == data.dwObjectID);
                var info = (AircraftInfo)data.dwData[0];
                aircraft?.Update(info);
            }
        }

        /// <summary>
        /// Processes any exceptions recieved from the sim
        /// </summary>
        /// <param name="sender">The open simconnect connection</param>
        /// <param name="data">The exception data structure</param>
        private static void SimConnect_OnRecvException(SimConnect sender, SIMCONNECT_RECV_EXCEPTION data)
        {
            Console.WriteLine("Exception: " + ((SIMCONNECT_EXCEPTION)data.dwException).ToString());
        }

        /// <summary>
        /// Processes any recieved airport lists
        /// </summary>
        /// <param name="sender">The open simconnect connection</param>
        /// <param name="data">The Airport lsit data structure</param>
        private void SimConnect_OnRecvAirportList(SimConnect sender, SIMCONNECT_RECV_AIRPORT_LIST data)
        {
            Console.WriteLine("Airport list:");
            Dump(data);
            DumpArray(data.rgData);
        }

        /// <summary>
        /// Processes new aircraft and sets up the info request loop
        /// </summary>
        /// <param name="sender">The open simconnect connection</param>
        /// <param name="data">The data structure of the new object</param>
        private void SimConnect_OnRecvAssignedObjectId(SimConnect sender, SIMCONNECT_RECV_ASSIGNED_OBJECT_ID data)
        {
            switch ((DATA_REQUESTS)data.dwRequestID)
            {
                case DATA_REQUESTS.NEW_AIRCRAFT:
                    Aircraft a = tentativeAircraft.Dequeue();
                    simulation.AddAircraft(new Aircraft(data.dwObjectID, a.Name, simulation, this, simulation.CurrentAirport, a.Latitude, a.Longitude));
                    // set up loop to start asking for things
                    simConnect.RequestDataOnSimObject((DATA_REQUESTS)(aircraftStartingID + data.dwObjectID), DEFINITIONS.AircraftInfo, data.dwObjectID, SIMCONNECT_PERIOD.SECOND, SIMCONNECT_DATA_REQUEST_FLAG.DEFAULT, 0, 0, 0);
                    Console.WriteLine("New aircraft made");
                    break;
            }
        }

        /// <summary>
        /// Converts a Node to a simconnect waypoint
        /// </summary>
        /// <param name="point">A Node</param>
        /// <returns>The node represented by a SIMCONNECT_DATA_WAYPOINT</returns>
        private static SIMCONNECT_DATA_WAYPOINT ConvertToSimConnect(Node point)
        {
            var newPoint = new SIMCONNECT_DATA_WAYPOINT
            {
                Latitude = point.Latitude,
                Longitude = point.Longitude,
            };

            switch(point)
            {
                case TaxiNode n:
                    newPoint.Flags = (uint)SIMCONNECT_WAYPOINT_FLAGS.ON_GROUND;
                    break;
                case RunwayNode n:
                    newPoint.Flags = (uint)SIMCONNECT_WAYPOINT_FLAGS.ON_GROUND;
                    break;
                case FlightNode n:
                    newPoint.Flags = (uint)SIMCONNECT_WAYPOINT_FLAGS.ALTITUDE_IS_AGL;
                    newPoint.Altitude = n.Altitude;
                    break;
                default:
                    throw new Exception("Unknown Node Type, cannot convert");
            }

            return newPoint;
        }

        /// <summary>
        /// Dumps an array for data to the screen (for debugging)
        /// </summary>
        /// <param name="rgData">The array to dump</param>
        private void DumpArray(Array rgData)
        {
            foreach (Object item in rgData)
            {
                Dump(item);
            }
        }

        /// <summary>
        /// Dumps an object to the console
        /// </summary>
        /// <param name="item">The object to dump</param>
        private static void Dump(Object item)
        {
            String s = "";
            foreach (System.Reflection.FieldInfo f in item.GetType().GetFields())
            {
                if (!f.FieldType.IsArray)
                {
                    if (f.Name == "Icao")
                    {
                        if (((String)f.GetValue(item))[0] != 'K')
                            return;
                    }

                    s += "  " + f.Name + ": " + f.GetValue(item);
                }
            }
            Console.WriteLine(s);
        }

        /// <summary>
        /// Handles the handshake recieved form the sim
        /// </summary>
        /// <param name="sender">The open simconnect connection</param>
        /// <param name="data">Information sent by the sim to say hi</param>
        private void SimConnect_OnRecvOpen(SimConnect sender, SIMCONNECT_RECV_OPEN data)
        {
            IsConnected = true;
            MainForm.Instance.ConnectedtoP3D = true;
            Console.WriteLine("Connected to P3D");
        }

        /// <summary>
        /// Handles the disconnection message recieved from the sim
        /// </summary>
        /// <param name="sender">The active simconnect connection</param>
        /// <param name="data">Any data sent by the sim as a result of closing the connection</param>
        private void SimConnect_OnRecvQuit(SimConnect sender, SIMCONNECT_RECV data)
        {
            IsConnected = false;
            MainForm.Instance.ConnectedtoP3D = false;
            CloseConnection();
            Console.WriteLine("Disconnected from P3D");
        }
    }
}