﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Speech.Recognition;
using Tower_Trainer.Properties;
using System.IO;
using System.Speech.Recognition.SrgsGrammar;

namespace Tower_Trainer
{
     public partial class SpeechForm : Form
     {
          //Main speech recognition engine, pass in a grammar/s and create
          //an event handler for the engine to recognize that grammar
          SpeechRecognitionEngine sre = new SpeechRecognitionEngine();

          //phraseology file found in the Environment.CurrentDirectory\xml folder
          const string PHRASEOLOGY_FILE = "phraseology.grxml";

          public SpeechForm()
          {
               InitializeComponent();
          }
          
          private void SpeechForm_Load(object sender, EventArgs e)
          {
               //Combine adds the '\' to the given strings. Environment.CurrentDirectory located 
               //in bin\debug in visual studio
               string path = Path.Combine(Environment.CurrentDirectory, "xml", PHRASEOLOGY_FILE);
               Grammar grammar = new Grammar(path);

               sre.LoadGrammarAsync(grammar); //not gonna suspend current thread
               sre.SetInputToDefaultAudioDevice(); //sets the mic to the default device

               sre.SpeechRecognized += Sre_SpeechRecognized;
          }

          private void Sre_SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
          {
               output_box.Text = e.Result.Semantics.Value.ToString();
               output_box.Text += "\nconfidence = " + e.Result.Confidence;


          }

          
          private void start_btn_Click(object sender, EventArgs e)
          {
               sre.RecognizeAsync(RecognizeMode.Multiple);
               start_btn.Enabled = false;
               end_btn.Enabled = true;
          }

          private void end_btn_Click(object sender, EventArgs e)
          {
               sre.RecognizeAsyncStop();
               start_btn.Enabled = true;
               end_btn.Enabled = false;
          }
     }
}
