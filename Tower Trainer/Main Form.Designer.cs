﻿namespace Tower_Trainer
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.txtStatus = new System.Windows.Forms.TextBox();
            this.lstFlightTypes = new System.Windows.Forms.ListBox();
            this.lblP3DStatus = new System.Windows.Forms.Label();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.debugToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.debugConsoleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.buttonPTT = new System.Windows.Forms.Button();
            this.SRTextBox = new System.Windows.Forms.RichTextBox();
            this.SRTextLabel = new System.Windows.Forms.Label();
            this.txtStatusLabel = new System.Windows.Forms.Label();
            this.menuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnStart
            // 
            this.btnStart.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btnStart.Location = new System.Drawing.Point(0, 305);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(91, 39);
            this.btnStart.TabIndex = 0;
            this.btnStart.Text = "Start Training";
            this.btnStart.UseVisualStyleBackColor = false;
            this.btnStart.Click += new System.EventHandler(this.BtnStart_Click);
            // 
            // btnStop
            // 
            this.btnStop.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btnStop.Enabled = false;
            this.btnStop.Location = new System.Drawing.Point(97, 305);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(91, 39);
            this.btnStop.TabIndex = 1;
            this.btnStop.Text = "Stop Training";
            this.btnStop.UseVisualStyleBackColor = false;
            this.btnStop.Click += new System.EventHandler(this.BtnStop_Click);
            // 
            // txtStatus
            // 
            this.txtStatus.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.txtStatus.Location = new System.Drawing.Point(418, 92);
            this.txtStatus.MaxLength = 999999999;
            this.txtStatus.Multiline = true;
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.ReadOnly = true;
            this.txtStatus.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtStatus.Size = new System.Drawing.Size(160, 164);
            this.txtStatus.TabIndex = 2;
            this.txtStatus.TextChanged += new System.EventHandler(this.txtStatus_TextChanged);
            // 
            // lstFlightTypes
            // 
            this.lstFlightTypes.FormattingEnabled = true;
            this.lstFlightTypes.Items.AddRange(new object[] {
            "VFR Departures"});
            this.lstFlightTypes.Location = new System.Drawing.Point(861, 146);
            this.lstFlightTypes.Name = "lstFlightTypes";
            this.lstFlightTypes.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lstFlightTypes.Size = new System.Drawing.Size(157, 56);
            this.lstFlightTypes.TabIndex = 4;
            this.lstFlightTypes.Visible = false;
            this.lstFlightTypes.SelectedIndexChanged += new System.EventHandler(this.lstFlightTypes_SelectedIndexChanged);
            // 
            // lblP3DStatus
            // 
            this.lblP3DStatus.AutoSize = true;
            this.lblP3DStatus.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.lblP3DStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblP3DStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblP3DStatus.ForeColor = System.Drawing.Color.Red;
            this.lblP3DStatus.Location = new System.Drawing.Point(149, 29);
            this.lblP3DStatus.Name = "lblP3DStatus";
            this.lblP3DStatus.Size = new System.Drawing.Size(159, 28);
            this.lblP3DStatus.TabIndex = 5;
            this.lblP3DStatus.Text = "Not Connected";
            this.lblP3DStatus.Click += new System.EventHandler(this.lblP3DStatus_Click);
            // 
            // menuStrip
            // 
            this.menuStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.optionsToolStripMenuItem,
            this.helpToolStripMenuItem,
            this.debugToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(579, 24);
            this.menuStrip.TabIndex = 6;
            this.menuStrip.Text = "menuStrip";
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.optionsToolStripMenuItem.Text = "Options";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // debugToolStripMenuItem
            // 
            this.debugToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.debugConsoleToolStripMenuItem});
            this.debugToolStripMenuItem.Name = "debugToolStripMenuItem";
            this.debugToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.debugToolStripMenuItem.Text = "Debug";
            // 
            // debugConsoleToolStripMenuItem
            // 
            this.debugConsoleToolStripMenuItem.Name = "debugConsoleToolStripMenuItem";
            this.debugConsoleToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.debugConsoleToolStripMenuItem.Text = "Debug Console";
            this.debugConsoleToolStripMenuItem.Click += new System.EventHandler(this.DebugConsoleToolStripMenuItem_Click);
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.numericUpDown1.Location = new System.Drawing.Point(72, 267);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            120,
            0,
            0,
            0});
            this.numericUpDown1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(52, 20);
            this.numericUpDown1.TabIndex = 7;
            this.numericUpDown1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(43, 247);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 17);
            this.label1.TabIndex = 8;
            this.label1.Text = "Aircraft Per Hour";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(131, 28);
            this.label2.TabIndex = 9;
            this.label2.Text = "P3D Status:";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1;
            this.timer1.Tick += new System.EventHandler(this.Timer1_Tick);
            // 
            // buttonPTT
            // 
            this.buttonPTT.BackColor = System.Drawing.Color.Red;
            this.buttonPTT.ForeColor = System.Drawing.Color.Black;
            this.buttonPTT.Location = new System.Drawing.Point(504, 24);
            this.buttonPTT.Name = "buttonPTT";
            this.buttonPTT.Size = new System.Drawing.Size(74, 41);
            this.buttonPTT.TabIndex = 10;
            this.buttonPTT.Text = "Microphone Active";
            this.buttonPTT.UseVisualStyleBackColor = false;
            this.buttonPTT.Click += new System.EventHandler(this.buttonPTT_Click);
            // 
            // SRTextBox
            // 
            this.SRTextBox.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.SRTextBox.Location = new System.Drawing.Point(418, 290);
            this.SRTextBox.Name = "SRTextBox";
            this.SRTextBox.Size = new System.Drawing.Size(160, 105);
            this.SRTextBox.TabIndex = 11;
            this.SRTextBox.Text = "";
            this.SRTextBox.TextChanged += new System.EventHandler(this.SRTextBox_TextChanged);
            // 
            // SRTextLabel
            // 
            this.SRTextLabel.AutoSize = true;
            this.SRTextLabel.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.SRTextLabel.ForeColor = System.Drawing.SystemColors.InfoText;
            this.SRTextLabel.Location = new System.Drawing.Point(419, 274);
            this.SRTextLabel.Name = "SRTextLabel";
            this.SRTextLabel.Size = new System.Drawing.Size(64, 13);
            this.SRTextLabel.TabIndex = 12;
            this.SRTextLabel.Text = "SRText Box";
            // 
            // txtStatusLabel
            // 
            this.txtStatusLabel.AutoSize = true;
            this.txtStatusLabel.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.txtStatusLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtStatusLabel.Location = new System.Drawing.Point(419, 76);
            this.txtStatusLabel.Name = "txtStatusLabel";
            this.txtStatusLabel.Size = new System.Drawing.Size(74, 13);
            this.txtStatusLabel.TabIndex = 13;
            this.txtStatusLabel.Text = "Activity Status";
            this.txtStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txtStatusLabel.Click += new System.EventHandler(this.txtStatusLabel_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.BackgroundImage = global::Tower_Trainer.Properties.Resources.tower;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(579, 396);
            this.Controls.Add(this.txtStatusLabel);
            this.Controls.Add(this.SRTextLabel);
            this.Controls.Add(this.SRTextBox);
            this.Controls.Add(this.buttonPTT);
            this.Controls.Add(this.lblP3DStatus);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.numericUpDown1);
            this.Controls.Add(this.lstFlightTypes);
            this.Controls.Add(this.txtStatus);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.menuStrip);
            this.DoubleBuffered = true;
            this.MainMenuStrip = this.menuStrip;
            this.Name = "MainForm";
            this.Text = "Tower Trainer";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.TextBox txtStatus;
        private System.Windows.Forms.ListBox lstFlightTypes;
        private System.Windows.Forms.Label lblP3DStatus;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem debugToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem debugConsoleToolStripMenuItem;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Timer timer1;
        public System.Windows.Forms.Button buttonPTT;
        private System.Windows.Forms.RichTextBox SRTextBox;
        private System.Windows.Forms.Label SRTextLabel;
        private System.Windows.Forms.Label txtStatusLabel;
    }
}

