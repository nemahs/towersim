﻿using System;

namespace Tower_Trainer
{
    /// <summary>
    /// Vector class for doing MATH
    /// </summary>
    public class Vector
    {
        public double X { get; private set; }
        public double Y { get; private set; }

        public Vector (double x, double y)
        {
            X = x;
            Y = y;
        }

        /// <summary>
        /// Adds two vectors together
        /// </summary>
        /// <param name="v">A vector to add to this</param>
        /// <returns>The resultant vector</returns>
        public Vector Add(Vector v)
        {
            return new Vector(X + v.X, Y + v.Y);
        }

        /// <summary>
        /// Subtracts two vectors
        /// Ex. a.Subtract(b) means a - b
        /// </summary>
        /// <param name="v">The vector to subtract</param>
        /// <returns>The resultant vector</returns>
        public Vector Subtract(Vector v)
        {
            return new Vector(X - v.X, Y - v.Y);
        }


        /// <summary>
        /// Normalizes a vector (a vector with a magnitude of 1)
        /// </summary>
        /// <returns>The normalized vector</returns>
        public Vector Normalize()
        {
            double magnitude = Math.Sqrt(Math.Pow(X, 2) + Math.Pow(Y, 2));
            return new Vector(X / magnitude, Y / magnitude);
        }

        /// <summary>
        /// Returns the heading of the vector
        /// NOTE: This heading is a compass heading, not an angle measurement
        /// ex. A vector pointing up has a heading of 0, right is 90, down is 180, left is 270
        /// </summary>
        public double Heading
        {
            get
            {
                double heading = Math.Atan2(Y, X) * 180 / Math.PI;
                heading -= 90;
                heading = 360 - heading;

                if (heading >= 360)
                    heading -= 360;

                return heading;
            }
        }

    }
}