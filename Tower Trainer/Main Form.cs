﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tower_Trainer.P3D;
using static Tower_Trainer.P3D.Connector;

namespace Tower_Trainer
{
    public partial class MainForm : Form
    {
        public static MainForm Instance;
        private readonly IConnector connector;
        private readonly Simulation simulation;
        private Debug_Console console;

        public bool ConnectedtoP3D
        {
            set
            {
                if (value)
                {
                    lblP3DStatus.Text = "Connected";
                    lblP3DStatus.ForeColor = System.Drawing.Color.Green;
                }
                else
                {
                    lblP3DStatus.Text = "Disconnected";
                    lblP3DStatus.ForeColor = System.Drawing.Color.Red;
                }
            }
        }

        public int AircraftPerHour
        {
            get { return (int)numericUpDown1.Value; }
        }

        public List<string> FlightType
        {
            get
            {
                var results = new List<string>();
                foreach (object o in lstFlightTypes.SelectedItems)
                {
                    results.Add((string)o);
                }

                return results;
            }
        }

        public MainForm()
        {
            InitializeComponent();
            Instance = this;
            simulation = new Simulation(this);
            connector = simulation.connector;
            console = new Debug_Console(simulation, connector);

            //testRoute();
            ConnectToSimAsync();
            var textbox = new List<Control>
            {
                txtStatus
            };
            using (var multiBoxWriter = new MultiBoxWriter(textbox, Console.Out))
            {
                Console.SetOut(multiBoxWriter);
            }
        }

        private async void ConnectToSimAsync()
        {
            while (!connector.IsConnected)
            {
                connector.OpenConnection(this.Handle);
                await Task.Delay(1000);
            }
        }

        protected override void DefWndProc(ref Message m)
        {
            if (m.Msg == WM_USER_SIMCONNECT)
            {
                connector.ReceiveMessage();
            }
            else
            {
                base.DefWndProc(ref m);
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (connector.IsConnected)
                connector.CloseConnection();
            console.Dispose();
        }

        private void BtnStart_Click(object sender, EventArgs e)
        {
            try
            {
                simulation.Start();
                btnStart.Enabled = false;
                btnStop.Enabled = true;
            }
            catch (Exception ex) { }
        }

        private void DebugConsoleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            console.Show();
        }

        private void BtnStop_Click(object sender, EventArgs e)
        {
            btnStop.Enabled = false;
            btnStart.Enabled = true;

            simulation.Stop();
            //Simulation.Instance.ServiceSR.RecognizeAsyncStop();
        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            if (connector.IsConnected)
            {
                timer1.Enabled = false;
            }
            else
            {
                connector.OpenConnection(this.Handle);
            }
        }

        public void ChangeText(string text)
        {
            SRTextBox.Text = text;
        }

        public delegate void SetColorChangeCallback(Color color);

        public void ChangeButtonColor(Color color)
        {
            if (buttonPTT.InvokeRequired)
            {
                var callback = new SetColorChangeCallback(ChangeButtonColor);
                Invoke(callback, new object[] { color });
            }
            else
            {
                buttonPTT.BackColor = color;
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void lstFlightTypes_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }

        private void buttonPTT_Click(object sender, EventArgs e)
        {

        }

        private void SRTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtStatus_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtStatusLabel_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void lblP3DStatus_Click(object sender, EventArgs e)
        {

        }
    }
}