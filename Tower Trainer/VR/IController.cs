﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tower_Trainer.VR
{
	interface IController <T> 
	{
          event EventHandler<T> TriggerHold;
	}
}
