﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX.XInput;
using System.Threading;

namespace Tower_Trainer.VR
{

     class TriggerHoldEventArgs : EventArgs {
     
          public bool TrigHeld{private set; get; }
          public TriggerHoldEventArgs(bool x) {
               TrigHeld = x;
          }
     }

	class XboxController : IController <TriggerHoldEventArgs> 
	{
		Controller controller;
		Gamepad gamepad;
		Gamepad previousState;         

		bool connected = false; // Keeps track of whether the controller is connected	
          public event EventHandler<TriggerHoldEventArgs> TriggerHold;
          protected virtual void OnTriggerHoldRequested(TriggerHoldEventArgs e)
          {
               TriggerHold?.Invoke(this, e);
          }
          public XboxController()
		{
			var controllers = new[] { new Controller(UserIndex.One), new Controller(UserIndex.Two),
				new Controller(UserIndex.Three), new Controller(UserIndex.Four) };

			foreach (var selectControler in controllers)
			{
				if (selectControler.IsConnected)
				{
					controller = selectControler;
					break;
				}
			}

			if (controller == null)
			{
				Console.WriteLine("No XInput controller installed");
			}

			gamepad = new Gamepad();
			Task pollController = Task.Run((Action)Update);

		}

          

          void Update()
		{
            if (controller == null)
                return;
			int delay = 1;
			previousState = controller.GetState().Gamepad;

			while (true)
			{
				gamepad = controller.GetState().Gamepad;

				connected = controller.IsConnected;

				if (!connected)
				{
					Thread.Sleep(delay);
					continue;
				}

                    if (gamepad.RightTrigger >= 150 &&
                         previousState.RightTrigger < 150)
                    {
                         
                         TriggerHoldEventArgs xEvent = new TriggerHoldEventArgs(true);
                         OnTriggerHoldRequested(xEvent);                         
                    }

                    else if (gamepad.RightTrigger < 150 &&
                         previousState.RightTrigger >= 150)
                    {
                         TriggerHoldEventArgs xEvent = new TriggerHoldEventArgs(false);
                         OnTriggerHoldRequested(xEvent);                         
                    }

                    previousState = gamepad;
				Thread.Sleep(delay);
			}
		}
	}
}
