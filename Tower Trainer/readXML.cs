﻿using Tower_Trainer.Airport_Information;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Tower_Trainer
{
    /// <summary>
    /// Parses an FSX/P3D XMl file to create an airport for the program to use
    /// </summary>
    class ReadXML
    {

        const string airportCode = "KMLB";

        /// <summary>
        /// Loads the airport listed in airportCode from the default file 
        /// </summary>
        /// <returns>An Airport representation of the airport listed in airportCode</returns>
        public static Airport collectData()
        {
            XElement root = XElement.Load("APX26220.bgl.xml");
            IEnumerable<XElement> airport =
                from el in root.Elements("Airport")
                where (string)el.Attribute("ident") == airportCode
                select el;
            Dictionary<int, TaxiNode> taxiNodes = taxiwayPoint(airport);
            Dictionary<int, ParkingNode> parkingNode = taxiwayParking(airport);
            BiDictionary<int,string> dictionary = taxiName(airport);
            List<Link> links = taxiwayPath(airport, taxiNodes);
            Dictionary<string, RunwayNode> runways = runwayStart(airport);

            double lat = (double)airport.Attributes("lat").First();
            double lon = (double)airport.Attributes("lon").First();


            return new Airport(lat, lon, runways, taxiNodes, links, parkingNode, dictionary);
        }

        /// <summary>
        /// Parses an airport XMl to retrieve all the taxiway nodes
        /// </summary>
        /// <param name="airport">An FSX XMl representation of an airport</param>
        /// <returns>A Dictionary of node ids and TaxiNodes</returns>
        private static Dictionary<int, TaxiNode> taxiwayPoint(IEnumerable<XElement> airport)
        {
            Dictionary<int, TaxiNode> nodes = new Dictionary<int, TaxiNode>();
            IEnumerable<XElement> points =
               from el in airport.Elements("TaxiwayPoint")
               select el;

            foreach (XElement el in points)

            {
                int index = (int)el.Attribute("index");
                double lat = (double)el.Attribute("lat"), lon = (double)el.Attribute("lon");
                TaxiNode.NodeType type = TaxiNode.NodeType.NORMAL;
                bool orientation = false;
                switch ((string)el.Attribute("type"))
                {
                    case "NORMAL":
                        type = TaxiNode.NodeType.NORMAL;
                        break;
                    case "HOLD_SHORT":
                        if ((string)el.Attribute("orientation") == "FORWARD")
                        {
                            orientation = true;
                        }
                        type = TaxiNode.NodeType.HOLD_SHORT;
                        break; 
                }
                TaxiNode current = new TaxiNode(lat, lon, type, orientation);
                nodes.Add(index, current);
            }
            return nodes;
        }
        
        /// <summary>
        /// Parses an airport XMl to retrieve all the runway nodes
        /// </summary>
        /// <param name="airport">An FSX XML representation of an airport</param>
        /// <returns>A dictionary of runway names and RunwayNodes</returns>
        private static Dictionary<string, RunwayNode> runwayStart(IEnumerable<XElement> airport)
        {
            Dictionary<string, RunwayNode> runway = new Dictionary<string, RunwayNode>();
            IEnumerable<XElement> points =
               from el in airport.Elements("Start")
               select el;
            foreach (XElement el in points)
            {
                string number = ((int)el.Attribute("number")).ToString();
                switch ((string)el.Attribute("designator"))
                {
                    case "RIGHT":
                        number += "R";
                        break;
                    case "LEFT":
                        number += "L";
                        break;
                    case "NONE":
                        break;
                }
                RunwayNode runwayNode = new RunwayNode((double)el.Attribute("lat"), (double)el.Attribute("lon"), (double)el.Attribute("heading"));
                runway.Add(number, runwayNode);
            }
            return runway;
        }

        /// <summary>
        /// Parses an airport XML to retrieve all the parking locations
        /// </summary>
        /// <param name="airport">An FSX XMl representation of an airport</param>
        /// <returns>A dictionary of node ids and ParkingNodes</returns>
        public static Dictionary<int, ParkingNode> taxiwayParking(IEnumerable<XElement> airport)
        {
            Dictionary<int, ParkingNode> nodes = new Dictionary<int, ParkingNode>();
            IEnumerable<XElement> points =
               from el in airport.Elements("TaxiwayParking")
               select el;
            foreach (XElement el in points)
            {
                int index = (int)el.Attribute("index");
                double lat = (double)el.Attribute("lat"), lon = (double)el.Attribute("lon"), heading = (double)el.Attribute("heading");

                ParkingNode.ParkingType type = ParkingNode.ParkingType.GATE_LARGE;
                switch ((string)el.Attribute("type"))
                {
                    case "RAMP_GA_LARGE":
                        type = ParkingNode.ParkingType.GA_LARGE;
                        break;
                    case "RAMP_GA_MEDIUM":
                        type = ParkingNode.ParkingType.GA_MEDIUM;
                        break;
                    case "RAMP_GA_SMALL":
                        type = ParkingNode.ParkingType.GA_SMALL;
                        break;
                }
                ParkingNode current = new ParkingNode(lat, lon, type, heading);
                nodes.Add(index, current); 
            }
            return nodes;
        }
    
        /// <summary>
        /// Parses an XML representation of an airport to retrieve the taxiway names
        /// </summary>
        /// <param name="airport">An FSX XML representation of an airport</param>
        /// <returns>A bi-directional dictionary mapping taxiway ids to taxiway names</returns>
        public static BiDictionary<int, string> taxiName(IEnumerable<XElement> airport)
        {
            BiDictionary<int, string> dictionary = new BiDictionary<int, string>();
            IEnumerable<XElement> points =
               from el in airport.Elements("TaxiName")
               select el;
            foreach (XElement el in points)
            {
                int index = (int)el.Attribute("index");
                string name = (string)el.Attribute("name");
                dictionary.Add(index, name);
            }
            return dictionary;
        }
        public static List<Link> taxiwayPath(IEnumerable<XElement> airport, Dictionary<int, TaxiNode> nodes)
        {
            List<Link> links = new List<Link>();
            IEnumerable<XElement> points =
               from el in airport.Elements("TaxiwayPath")
               select el;
            
            foreach (XElement el in points)
            {
                int start = (int)el.Attribute("start");
                int end = (int)el.Attribute("end");
                int number = -1;
                string desig = (string)el.Attribute("designator");
                Link.LinkType type = Link.LinkType.RUNWAY;
                switch ((string)el.Attribute("type"))
                {
                    case "RUNWAY":
                        type = Link.LinkType.RUNWAY;
                        number = (int)el.Attribute("number");
                        desig = desig.First().ToString();
                        break;
                    case "TAXI":
                        type = Link.LinkType.TAXIWAY;
                        number = (int)el.Attribute("name");
                        break;
                    
                    case "PATH":
                        type = Link.LinkType.PATH;
                        break;
                    case "PARKING":
                        type = Link.LinkType.PARKING;
                        break;
                    case "VEHICLE":
                        type = Link.LinkType.VEHICLE;
                        break;
                }
                Link current = new Link(start, end, type, number, desig);
                links.Add(current);
            }
            return links;
        }

    }
}
