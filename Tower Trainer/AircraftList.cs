﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tower_Trainer
{
    /// <summary>
    /// Helper class to let us access aircraft by name or id
    /// </summary>
    class AircraftList
    {
        private List<Aircraft> list = new List<Aircraft>();

        /// <summary>
        /// Adds and aircraft to the list
        /// </summary>
        /// <param name="a">The aircraft to add</param>
        public void Add(Aircraft a)
        {
            if (!list.Contains(a))
            {
                list.Add(a);
            }
        }

        /// <summary>
        /// Removes an aircraft from the list
        /// </summary>
        /// <param name="name">The callsign of the aircraft (ex. N123AB)</param>
        public void RemoveByName(String name)
        {
            list.RemoveAll(x => x.Name == name);
        }

        /// <summary>
        /// Removes an aircraft from the list
        /// </summary>
        /// <param name="id">The id of the aircraft (NOT ITS INDEX In THE LIST)</param>
        public void RemoveByID(uint id)
        {
            list.RemoveAll(x => x.ID == id);
        }

        /// <summary>
        /// Gets an aircraft by its id
        /// </summary>
        /// <param name="id">The id of the aircraft (NOT ITS INDEX)</param>
        /// <returns>An Aircraft</returns>
        public Aircraft GetByID(uint id)
        {
            return list.Find(x => x.ID == id);
        }

        /// <summary>
        /// Gets an aircraft by its callsign
        /// </summary>
        /// <param name="name">The aicraft's callsign (ex. N123AB)</param>
        /// <returns>An Aircraft</returns>
        public Aircraft GetByName(String name)
        {
            return list.Find(x => x.Name == name);
        }
    }
}
