﻿
// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc.

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "CC0001:You should use 'var' whenever possible.", Justification = "<Pending>", Scope = "member", Target = "~M:Tower_Trainer.Aircraft.ParseRoute(System.String)~Microsoft.FlightSimulator.SimConnect.SIMCONNECT_DATA_WAYPOINT[]")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "CC0001:You should use 'var' whenever possible.", Justification = "<Pending>", Scope = "member", Target = "~M:Tower_Trainer.Aircraft.ConvertToSimConnect(System.Int32)~Microsoft.FlightSimulator.SimConnect.SIMCONNECT_DATA_WAYPOINT[]")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "CC0001:You should use 'var' whenever possible.", Justification = "<Pending>", Scope = "member", Target = "~P:Tower_Trainer.Aircraft.Route")]

