﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Tower_Trainer.Airport_Information;
using Tower_Trainer.P3D;

namespace Tower_Trainer
{
    public partial class Debug_Console : Form
    {
        private delegate void TextCommandCallback(string[] args);
        private readonly ISimulation simulation;
        private readonly IConnector connector;
        private readonly Dictionary<string, TextCommandCallback> commands = new Dictionary<string, TextCommandCallback>();

        public Debug_Console(ISimulation simulation, IConnector connector)
        {
            InitializeComponent();
            this.simulation = simulation;
            this.connector = connector;
            commands.Add("create", CreateAircraft);
            commands.Add("delete", DeleteAircraft);
            commands.Add("taxi", TaxiAircraft);
            commands.Add("luaw", LineUpAircraft);
            commands.Add("takeoff", DepartAircraft);
            commands.Add("list", ListAircraft);
            commands.Add("stop", Stop);
            commands.Add("go", Go);
            commands.Add("continue", Continue);
        }

        private void TxtInput_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                ParseCommand(txtInput.Text);
                txtInput.Text = "";
            }
        }

        private void ParseCommand(string command)
        {
            var split = command.Split(' ');

            if (commands.TryGetValue(split[0].ToLower(), out TextCommandCallback func))
            {
                var args = split.Skip(1).ToArray();
                func(args);
            }
        }

        private bool HasCorrectNumberOfArgs(int numOfArgs, string[] args, string usage)
        {
            if (args.Length < numOfArgs)
            {
                txtOutput.AppendText($"Incorrect number of args. Usage:\n{usage}\n");
                return false;
            }

            return true;
        }

        private Aircraft GetAircraftByNameOrNum(string item)
        {
            if (Int32.TryParse(item, out int result))
            {
                return result < simulation.CurrentAircraft.Count ? simulation.CurrentAircraft[result] : null;
            }
            else
            {
                return simulation.CurrentAircraft.Find(x => x.Name == item);
            }
        }

        /*************************************
        *              COMMANDS              *
        **************************************/

        private void CreateAircraft(string[] args)
        {
            if (HasCorrectNumberOfArgs(2, args, "create AIRCRAFT_NAME AIRCRAFT_TYPE"))
            {
                var r = new Random();
                simulation.CurrentAirport.ParkingSpots.TryGetValue(r.Next(1, 10), out ParkingNode node);
                var type = String.Join(" ", args.Skip(1));
                if (node != null)
                { 
                    connector.CreateAircraft(type, args[0], node);
                    txtOutput.AppendText($"Create message sent.\n{args[0]}");
                }
            }
        }

        private void DeleteAircraft(string[] args)
        {
            if (HasCorrectNumberOfArgs(1, args, "delete AIRCRAFT_NAME"))
            {
                var aircraft = GetAircraftByNameOrNum(args[0]);
                if (aircraft != null)
                {
                    connector.DestroyAircraft(aircraft);
                    txtOutput.AppendText($"Deleted {aircraft.Name}\n");
                }
            }
        }

        private void TaxiAircraft(string[] args)
        {
            if (HasCorrectNumberOfArgs(2, args, "taxi AIRCRAFT_NAME ROUTE  . ROUTE must be comma separated"))
            {
                var aircraft = GetAircraftByNameOrNum(args[0]);
                if (aircraft != null) { aircraft.SetRoute(args[1]); }
            }
        }

        private void LineUpAircraft(string[] args)
        {
            if (HasCorrectNumberOfArgs(2, args, "luaw AIRCRAFT_NAME RUNWAY"))
            {
                var aircraft = GetAircraftByNameOrNum(args[0]);
				var runway = args[1];
                aircraft?.LineUpAndWait(runway);
            }
        }

        private void DepartAircraft(string[] args)
        {
            if (HasCorrectNumberOfArgs(2, args, "takeoff AIRCRAFT_NAME RUNWAY"))
            {
                var aircraft = GetAircraftByNameOrNum(args[0]);
				var runway = args[1];
                aircraft?.Depart(runway);
            }
        }

        private void ListAircraft(string[] args)
        {
            txtOutput.AppendText($"Number of Aircraft: {simulation.CurrentAircraft.Count}\n");

            var i = 0;
            foreach (Aircraft aircraft in simulation.CurrentAircraft)
            {
                txtOutput.AppendText($"ID: {i}\tName: {aircraft.Name}\tStatus: {aircraft.Status}\n");
                i++;
            }
        }

        private void Stop(string[] args)
        {
            if (HasCorrectNumberOfArgs(1, args, "stop AIRCRAFT_NAME"))
            {
                var aircraft = GetAircraftByNameOrNum(args[0]);
                aircraft?.Stop();
            }
        }

        private void Go(string[] args)
        {
            if (HasCorrectNumberOfArgs(1, args, "go AIRCRAFT_NAME"))
            {
                var aircraft = GetAircraftByNameOrNum(args[0]);
                aircraft?.Go();
            }
        }

        private void Continue(string[] args)
        {
            if (HasCorrectNumberOfArgs(2, args, "continue AIRCRAFT_NAME RUNWAY"))
            {
                var aircraft = GetAircraftByNameOrNum(args[0]);
				var runway = args[1];
                aircraft?.ContinueTaxi(runway);
            }
        }
	}
}