﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace Tower_Trainer
{
    /// <summary>
    /// Provides a way to write to multiple UI controls, stdOut, and a log file with one call
    /// </summary>
    class MultiBoxWriter: TextWriter
    {
        private readonly IEnumerable<Control> textboxes;
        private readonly TextWriter stdOut;
        private readonly StreamWriter logfile;

        public MultiBoxWriter(IEnumerable<Control> textboxes, TextWriter stdOut)
        {
            this.textboxes = textboxes;
            this.stdOut = stdOut;
            var path = Path.Combine(Application.StartupPath, "logs");
            Directory.CreateDirectory(path);
            logfile = new StreamWriter(Path.Combine(path, $"{DateTime.Now.ToString("ddmmyyyy-HHmmss")}.log"));
        }

        public override Encoding Encoding
        {
            get { return Encoding.UTF32; }
        }

		delegate void WriteCallback(string value);

        public override void Write(string value)
        {
            foreach (Control textbox in textboxes)
            {
				// Allows for textbox control to be written into from multiple threads
				// https://docs.microsoft.com/en-us/dotnet/framework/winforms/controls/how-to-make-thread-safe-calls-to-windows-forms-controls
				if (textbox.InvokeRequired)
				{
					WriteCallback callback = new WriteCallback(Write);
					textbox.FindForm().Invoke(callback, new object[] { value });
				}

				else
				{
					textbox.Text += value;
				}
            }

            stdOut.Write(value);

            logfile.Write(value);
            logfile.Flush();
        }

        public override void Write(char value)
        {
            Write(value.ToString());
        }

    }
}
