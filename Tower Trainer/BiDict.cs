﻿using System;
using System.Collections.Generic;

/// <summary>
/// A 1 to 1 dictionary that lets you access items by either value
/// </summary>
/// <typeparam name="TFirst">Type of the first value</typeparam>
/// <typeparam name="TSecond">Type of the second value</typeparam>
public class BiDictionary<TFirst, TSecond>
{
    IDictionary<TFirst, TSecond> firstToSecond = new Dictionary<TFirst, TSecond>();
    IDictionary<TSecond, TFirst> secondToFirst = new Dictionary<TSecond, TFirst>();

    /// <summary>
    /// Adds a pair to the dictionary
    /// </summary>
    /// <param name="first">The first value</param>
    /// <param name="second">The second value</param>
    public void Add(TFirst first, TSecond second)
    {
        if (firstToSecond.ContainsKey(first) ||
            secondToFirst.ContainsKey(second))
        {
            throw new ArgumentException("Duplicate first or second");
        }
        firstToSecond.Add(first, second);
        secondToFirst.Add(second, first);
    }

    /// <summary>
    /// Gets a pair using the first value
    /// </summary>
    /// <param name="first">The value to search by</param>
    /// <param name="second">Will contain the second value if the pair is found</param>
    /// <returns>True if the pair exists, False otherwise</returns>
    public bool TryGetByFirst(TFirst first, out TSecond second)
    {
        return firstToSecond.TryGetValue(first, out second);
    }

    /// <summary>
    /// Gets a pair using the second value
    /// </summary>
    /// <param name="second">The value to search by</param>
    /// <param name="first">Will contain the first value if the pair is found</param>
    /// <returns>True if the pair exists, False otherwise</returns>
    public bool TryGetBySecond(TSecond second, out TFirst first)
    {
        return secondToFirst.TryGetValue(second, out first);
    }
}