This folder contains all of the grammars that will be used by the application.

Each new phrase should be placed in it's own grammar.

The name of the grammar must be added into the Grammars enum in speechform.

Shared grammars, like callsigns, are placed in the shared folder. The rule must have a public scope in order to be used.