﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tower_Trainer.Speech
{
    /// <summary>
    /// implement this if you wish to modify underlying TTS synthesizer
    /// </summary>
    interface IServiceTTS<T> where T:EventArgs
    {
        void SpeakAsync(string callsign, string speakString, int priority);
		string SpaceOut(string input);

        void Pause();
		void Resume();
		void Reset();
		event EventHandler<T> SpeakFinished;
	}
}
