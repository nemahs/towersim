﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tower_Trainer.Speech
{
    /// <summary>
    /// implement this if you wish to modify underlying sre
    /// </summary>
    interface IServiceSR<T> where T:EventArgs
    {

        void RecognizeAsync();
        void RecognizeAsyncStop();

        event EventHandler<T> OutputRequest;
        // EventHandler<U> TTSRequest;
    }
}
