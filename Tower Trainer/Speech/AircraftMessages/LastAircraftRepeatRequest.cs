﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tower_Trainer.Speech.AircraftMessages
{
	class LastAircraftRepeatRequest<T> : AircraftMessage<T> where T : EventArgs
	{
		public LastAircraftRepeatRequest(IServiceTTS<T> serviceTTS, Aircraft aircraft) : base(serviceTTS, aircraft)
		{
			priority = 10;
		}

		public override void Execute()
		{
			var spacedOutName = serviceTTS.SpaceOut(aircraft.Name);
			var speakString = $"Melbourne tower, was that for {spacedOutName}?";

			Schedule(speakString);
		}
	}
}
