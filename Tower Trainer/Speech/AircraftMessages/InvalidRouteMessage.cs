﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tower_Trainer.Speech.AircraftMessages
{
	class InvalidRouteMessage<T> : AircraftMessage<T> where T : EventArgs
	{
		public InvalidRouteMessage(IServiceTTS<T> serviceTTS, Aircraft aircraft) : base(serviceTTS, aircraft)
		{
			priority = 5;
		}

		public override void Execute()
		{
			// Have to space out callsigns for the lexicon to correctly enunciate it
			var spacedOutName = serviceTTS.SpaceOut(aircraft.Name);
			var speakString = $"{spacedOutName}, I cannot do that, please give better instructions";

			Schedule(speakString);
		}
	}
}
