﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tower_Trainer.Speech.AircraftMessages
{
	class NoMessage<T> : AircraftMessage<T> where T : EventArgs
	{
		public NoMessage() : base(null, null) { }

		public override void Execute()
		{

		}
	}
}
