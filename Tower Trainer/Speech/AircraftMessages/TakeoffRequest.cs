﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tower_Trainer.Speech.AircraftMessages
{
	class TakeoffRequest<T> : AircraftMessage<T> where T: EventArgs
	{
		public TakeoffRequest(IServiceTTS<T> serviceTTS, Aircraft aircraft) : base(serviceTTS, aircraft)
		{
			priority = 3;
		}

		public override void Execute()
		{
			var spacedOutName = serviceTTS.SpaceOut(aircraft.Name);
			var spacedOutDestinationRunway = serviceTTS.SpaceOut(aircraft.DestinationRunway);

			var speakString = $"{spacedOutName} is holding short of runway {spacedOutDestinationRunway}, ready for departure";
			Schedule(speakString);
		}
	}
}
