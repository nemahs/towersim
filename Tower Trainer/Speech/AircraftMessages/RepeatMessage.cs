﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tower_Trainer.Speech.AircraftMessages
{
	class RepeatMessage<T> : AircraftMessage<T> where T : EventArgs
	{
		public RepeatMessage(IServiceTTS<T> serviceTTS, Aircraft aircraft) : base(serviceTTS, aircraft)
		{
			priority = 5;
		}

		public override void Execute()
		{
			if (aircraft != null)
			{
				Schedule(aircraft.SpeakString);
			}
		}
	}
}
