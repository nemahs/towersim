﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tower_Trainer.Speech.AircraftMessages
{
	class TaxiRequest<T> : AircraftMessage<T> where T : EventArgs
	{

		public TaxiRequest(IServiceTTS<T> serviceTTS, Aircraft aircraft) : base(serviceTTS, aircraft)
		{
			priority = 1;
		}

		public override void Execute()
		{
			// Have to space out callsigns for the lexicon to correctly enunciate it
			var spacedOutName = serviceTTS.SpaceOut(aircraft.Name);
			var speakString = $"Melbourne Tower, {spacedOutName} is a Mooney Bravo on the {aircraft.Position}, " +
				$"request taxi for {aircraft.AircraftDirection.Name} departure";

			Schedule(speakString);
		}
	}
}
