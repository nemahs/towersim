﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tower_Trainer.Speech.AircraftMessages
{
    class UnableMessage<T> : AircraftMessage<T> where T : EventArgs
    {

        public UnableMessage(IServiceTTS<T> serviceTTS, Aircraft aircraft): 
			base(serviceTTS, aircraft)
        {
            priority = 10;
        }

        public override void Execute()
        {
            string reason;

            switch (aircraft.Status)
            {
                case Aircraft.AircraftStatus.Taxiing:
                    var spacedOutRunway = serviceTTS.SpaceOut(aircraft.DestinationRunway);
                    reason = $"unable, taxiing to {spacedOutRunway}";
                    break;

                case Aircraft.AircraftStatus.Departing:
                    reason = $"unable, departing airspace";
                    break;

                case Aircraft.AircraftStatus.HoldShort:
                    var shortOf = serviceTTS.SpaceOut(aircraft.ShortOf);
                    reason = $"unable, holding short of {shortOf}";
                    break;

                case Aircraft.AircraftStatus.InvalidRoute:
                    reason = $"unable, awaiting proper taxi instructions";
                    break;

                case Aircraft.AircraftStatus.LUAW:
                    spacedOutRunway = serviceTTS.SpaceOut(aircraft.DestinationRunway);
                    reason = $"unable, awaiting takeoff instructions from runway {spacedOutRunway}";
                    break;

                case Aircraft.AircraftStatus.ShortOfRunway:
                    spacedOutRunway = serviceTTS.SpaceOut(aircraft.DestinationRunway);
                    reason = $"unable, holding short of runway {spacedOutRunway}, ready to takeoff";
                    break;

                case Aircraft.AircraftStatus.Spawned:
                    reason = $"unable, awaiting taxiing instructions";
                    break;

                case Aircraft.AircraftStatus.Waiting:
                    reason = $"unable, there is an aircraft in the way";
                    break;

                default:
                    reason = "unable";
                    break;
            }

            var spacedOutName = serviceTTS.SpaceOut(aircraft.Name);
            var speakString = $"Melbourne Tower, {spacedOutName}, {reason}";
            Schedule(speakString);
        }
    }
}
