﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tower_Trainer.Speech.AircraftMessages
{
	abstract class AircraftMessage<T> where T:EventArgs
	{
		protected IServiceTTS<T> serviceTTS;
		protected Aircraft aircraft;
		protected int priority;
		
		public bool IsCancelled { get; private set; }

		public AircraftMessage(IServiceTTS<T> serviceTTS, Aircraft aircraft)
		{
			this.serviceTTS = serviceTTS;
			this.aircraft = aircraft;
			IsCancelled = false;
			priority = 1;
		}

		protected void Schedule(string speakString)
		{
			serviceTTS.SpeakAsync(callsign: aircraft.Name,speakString: speakString, priority: priority);
		}

		public abstract void Execute();
	}
}
