﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tower_Trainer.Speech.AircraftMessages
{
	class HoldingShortMessage<T> : AircraftMessage<T> where T : EventArgs
	{
		public HoldingShortMessage(IServiceTTS<T> serviceTTS, Aircraft aircraft) : base(serviceTTS, aircraft)
		{
			priority = 4;
		}

		public override void Execute()
		{
			// Have to space out callsigns for the lexicon to correctly enunciate it
			var spacedOutName = serviceTTS.SpaceOut(aircraft.Name);
			var spacedOutDestinationRunway = serviceTTS.SpaceOut(aircraft.DestinationRunway);
			var spacedOutHoldShort = serviceTTS.SpaceOut(aircraft.ShortOf);
			
			var speakString = $"{spacedOutName} is holding short of {spacedOutDestinationRunway} at {spacedOutHoldShort}";
			Schedule(speakString);
		}
	}
}
