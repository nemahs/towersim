﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tower_Trainer.Speech.AircraftMessages
{
    class AircraftRequest<T> : AircraftMessage<T> where T : EventArgs
    {
        public AircraftRequest(IServiceTTS<T> serviceTTS, Aircraft aircraft):
            base(serviceTTS, aircraft) { }

        public override void Execute()
        {
            AircraftMessage<T> message = new NoMessage<T>();

            switch (aircraft.Status)
            {
                case Aircraft.AircraftStatus.Spawned:
                    message = new TaxiRequest<T>(serviceTTS, aircraft);
                    break;

                case Aircraft.AircraftStatus.InvalidRoute:
                    message = new InvalidRouteMessage<T>(serviceTTS, aircraft);
                    break;

                case Aircraft.AircraftStatus.HoldShort:
                    message = new HoldingShortMessage<T>(serviceTTS, aircraft);
                    break;

                case Aircraft.AircraftStatus.ShortOfRunway:
                    message = new TakeoffRequest<T>(serviceTTS, aircraft);
                    break;

                case Aircraft.AircraftStatus.Departing:
                    message = new DepartingMessage<T>(serviceTTS, aircraft);
                    break;
            }

            message.Execute();
        }
    }
}
