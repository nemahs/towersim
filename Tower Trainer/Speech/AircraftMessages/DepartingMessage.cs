﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tower_Trainer.Speech.AircraftMessages
{
	class DepartingMessage<T> : AircraftMessage<T> where T: EventArgs
	{
		public DepartingMessage(IServiceTTS<T> serviceTTS, Aircraft aircraft) : base(serviceTTS, aircraft)
		{
			priority = 3;
		}

		public override void Execute()
		{
			var spacedOutName = serviceTTS.SpaceOut(aircraft.Name);
			var speakString = $"{spacedOutName} is leaving the airspace, changing frequency";
			Schedule(speakString);
		}
	}
}
