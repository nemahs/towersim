﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tower_Trainer.Speech.AircraftMessages
{
	class Readback<T> : AircraftMessage<T> where T : EventArgs
	{
		private Dictionary<string, string> context;

		public Readback(IServiceTTS<T> serviceTTS, Aircraft aircraft, 
            Dictionary<string, string> context): base(serviceTTS, aircraft)
		{
			this.context = context;
			priority = 5;
		}

		public override void Execute()
		{
            string readback = context["readback"];

            if (context.TryGetValue("callsign", out string callsign))
            {
                readback = readback.Replace(callsign, serviceTTS.SpaceOut(callsign));
            }

            if (context.TryGetValue("runway", out string runway))
            {
                readback = readback.Replace(runway, serviceTTS.SpaceOut(runway));
            }

            if (context.TryGetValue("taxiway", out string taxiway))
            {
                readback = readback.Replace(taxiway, serviceTTS.SpaceOut(taxiway));
            }

			Schedule(readback);
		}
	}
}
