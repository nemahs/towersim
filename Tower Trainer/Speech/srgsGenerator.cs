﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Speech.Recognition.SrgsGrammar;
using System.Text;
using System.Threading.Tasks;

namespace Tower_Trainer.Speech
{
     class srgsGenerator
     {
          void generator()
          {
               // Create a rule and give it the identifier "fruitList".
               SrgsRule fruitRule = new SrgsRule("fruitList");

               // Create a list of alternatives containing names of fruit 
               // and add the list to the rule.
               SrgsOneOf fruit = new SrgsOneOf(new string[] { "apple", "banana", "pear", "peach", "melon" });
               fruitRule.Add(fruit);
               fruitRule.Scope = SrgsRuleScope.Public;

               // Create an SrgsDocument instance, add the rule, and set it as the root rule.
               SrgsDocument document = new SrgsDocument(@"c:\temp\cities.grxml");
               document.Rules.Add(fruitRule);
               document.Root = fruitRule;
          }
     }
}
