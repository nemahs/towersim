﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Speech.Synthesis;
using System.IO;
using System.Globalization;
using System.Threading;
using C5;

namespace Tower_Trainer.Speech
{
    /// <summary>
    /// turns on speech recognition for when the 
    /// air traffic controller speaks
    /// </summary>
    public class SpeakFinishedEventArgs : EventArgs
    {
        public string Callsign { get; set; }
		public string SpeakString { get; set; }

		public SpeakFinishedEventArgs(string callsign, string speakString)
		{
			Callsign = callsign;
			SpeakString = speakString;
		}
    }

    class ServiceTTS : IServiceTTS<SpeakFinishedEventArgs>
    {
        class PrioritizedSpeech : IComparable<PrioritizedSpeech>
        {
			public string Callsign { get; private set; }
            public string SpeakString { get; private set; }
            int Priority { get; set; }

			// overloaded constructor of sorts
			public PrioritizedSpeech(string callsign, string speakString, int priority)
			{
				Setup(callsign, speakString, priority);
			}

			//public PrioritizedSpeech(string callsign, Prompt speakString, int priority)
			//{
			//	Setup(callsign, speakString, priority);
			//}

			private void Setup(string callsign, string speakString, int priority)
			{
				Callsign = callsign;
				SpeakString = speakString;
				Priority = priority;
			}

			public int CompareTo(PrioritizedSpeech other)
			{
				return Priority.CompareTo(other.Priority);
			}
		}

		private const int LOW_PRIORITY = 3;
		SpeechSynthesizer synth;
        IntervalHeap<PrioritizedSpeech> priorityQueue;

		// Used to find a handle for a prioritized speech using its callsign and delete
		// it from the priority queue
		Dictionary<string, IPriorityQueueHandle<PrioritizedSpeech>> speechLookup;
        Random rng;
		PrioritizedSpeech currentlySpeaking;
        TaskCompletionSource<object> ATCSpeakingDone, speakingDone;
        public event EventHandler<SpeakFinishedEventArgs> SpeakFinished;

        public ServiceTTS()
        {
            synth = new SpeechSynthesizer();
            rng = new Random();

            // used to stop the plane from speaking if the ATC is speaking
            ATCSpeakingDone = new TaskCompletionSource<object>();
			ATCSpeakingDone.SetResult(true);

            priorityQueue = new IntervalHeap<PrioritizedSpeech>();

			speechLookup = new Dictionary<string, IPriorityQueueHandle<PrioritizedSpeech>>();

			synth.SpeakCompleted += Synth_SpeakCompleted;

            // asynchronously call speak when responses are available and the previous speak was finished
            //pollResponse = new ActionBlock<PrioritizedSpeech>(PollAircraftResponses);
            Task.Run(PollAircraftResponses);

            initializeVoice();
        }

        private void Synth_SpeakCompleted(object sender, SpeakCompletedEventArgs e)
		{
			speakingDone.SetResult(null);
		}

		private void initializeVoice()
        {
            string pathToLexicons = Path.Combine(Environment.CurrentDirectory, "Speech", "xml", "pronunciation");
            string[] lexiconPaths = Directory.GetFiles(pathToLexicons, "*.pls");

            foreach (string lexiconPath in lexiconPaths)
            {
                synth.AddLexicon(new Uri(lexiconPath), "application/pls+xml");
            }

            /*foreach (InstalledVoice voice in synth.GetInstalledVoices())
            {
                MessageBox.Show(voice.VoiceInfo.Name);
            }*/

            foreach (InstalledVoice voice in synth.GetInstalledVoices())
            {

                if (voice.VoiceInfo.Culture.Equals(CultureInfo.GetCultureInfo("en-US")))
                {
                    synth.SelectVoice(voice.VoiceInfo.Name);
                    break;
                }
            }
            // Configure the audio output. 
            synth.SetOutputToDefaultAudioDevice();
            synth.Rate = -2;
        }

        private async Task PollAircraftResponses()
        {
			while (true)
			{
				do
				{
					// Async wait until ATC is finished speaking
					await ATCSpeakingDone.Task;

					// Wait a bit after the ATC finished speaking
					await Task.Delay(TimeSpan.FromSeconds(rng.Next(1) + 1));

				// Loops while the ATC is not done speaking
				} while (!ATCSpeakingDone.Task.IsCompleted);

				if (priorityQueue.Count > 0)
				{
					speakingDone = new TaskCompletionSource<object>();

					PrioritizedSpeech speech = priorityQueue.DeleteMax
						(out IPriorityQueueHandle<PrioritizedSpeech> handle);

					// Find and remove the callsign/handle pair from the speechLookup dictionary,
					// so that future additions into the dictionary by the same callsign won't try
					// to remove a non-existent handle
					var callsign = speech.Callsign;

					// handle may not exist if the speech has low priority (defined by the LOW_PRIORITY constant)
					if (speechLookup.ContainsKey(callsign))
						speechLookup.Remove(callsign);

					currentlySpeaking = speech;

					synth.SpeakAsync(currentlySpeaking.SpeakString);

					// Only way to await for SpeakAsync, trigged on SpeechComplete event
					await speakingDone.Task;
					currentlySpeaking = null;

					var e = new SpeakFinishedEventArgs(speech.Callsign, speech.SpeakString);
					OnSpeakFinished(e);

					// wait in between responses
					await Task.Delay(TimeSpan.FromSeconds(rng.Next(2) + 2));
				}

				else
				{
					// wait a second for the priority queue to fill up
					await Task.Delay(TimeSpan.FromSeconds(1));
				}
			}
        }

        /// <summary>
        /// schedule a new prioritized speach object which will be spoken after its removed from the priority queue
        /// </summary>
        /// <param name="callsign">The identifier for which aircraft will speak.</param>
        /// <param name="speakString">The string to be spoken by the aircraft.</param>
        /// <param name="priority"> determines the speakstring's placement in the priority queue.</param>       
        /// <remarks>
        /// called by aircraft messages
        /// </remarks>
        public void SpeakAsync(string callsign, string speakString, int priority)
        {
			// Try finding if this callsign is already trying to say something, and delete it
			speechLookup.TryGetValue(callsign, out IPriorityQueueHandle<PrioritizedSpeech> handle);

			if (handle != null)
			{
				priorityQueue.Delete(handle);
				speechLookup.Remove(callsign);
			}

			if (!string.IsNullOrEmpty(speakString))
			{
				PrioritizedSpeech speakTask = new PrioritizedSpeech(callsign, speakString, priority);

				// You can pass in a previously used but invalidated handle. Cause why not
				priorityQueue.Add(ref handle, speakTask);

				// if the priority of the message is sufficiently low, it means that it's something important
				// that the aircraft must say, and should therefore never be canceled
				if (priority > LOW_PRIORITY)
					speechLookup.Add(callsign, handle);

			}

        }

        /// <summary>
        /// Pauses all text-to-speech operations, including any that are currently being
        /// spoken (which are returned back into the queue).
        /// </summary>        
        public void Pause()
		{
			// create new task completion source which pauses the polling until
            // the resume method can set the result for it.
			ATCSpeakingDone = new TaskCompletionSource<object>();

            // Obtain the prompt, if any, that's currently being spoken
			Prompt prompt = synth.GetCurrentlySpokenPrompt();

			if (prompt != null && !prompt.IsCompleted)
			{
				SpeakAsync(currentlySpeaking.Callsign, currentlySpeaking.SpeakString, 5);
			}

			synth.SpeakAsyncCancelAll();
		}

        /// <summary>
        /// Resumes all text to speech operations currently queued.
        /// </summary>
        public void Resume()
		{
			// set the awaiting task as done, allowing polling to continue
			ATCSpeakingDone.SetResult(null);
			//synth.Resume();
        }
        
		public void Reset()
		{
			synth.SpeakAsyncCancelAll();
			priorityQueue = new IntervalHeap<PrioritizedSpeech>();
			speechLookup.Clear();
		}

        /// <summary>
        /// raises an event for when TTS synthesizer has been stopped or finished speaking.
        /// </summary>
        /// <param name="e">The SpeakFinishedEventArgs arguments.</param>
        /// <remark>
        /// method raises an event for all suscribers to consume.
        /// consumption of event involves invoking the delegate (suscriber) method if any exists.
        /// </remark>
        protected virtual void OnSpeakFinished(SpeakFinishedEventArgs e)
        {
            SpeakFinished?.Invoke(this, e);
        }

        /// <summary>
        /// spaces out any alpha numeric characters for proper recognition by the lexicon
        /// </summary>
        /// <param name="input">The string to be spaced out.</param>
        /// <returns>
        /// spaced out string
        /// </returns>
        /// <remarks>
        /// allows proper pronunciation of alpha numerics since lexicons
        /// can't recognize individual letters in words.
        /// result - lexicon sees "Bravo" instead of "B"
        /// </remarks>
        public string SpaceOut(string input)
		{
			if (string.IsNullOrEmpty(input)) return input;
			input = input.Aggregate(string.Empty, (c, i) => c + i + ' ');
			return input.Replace("A", "@"); // Lexicons cannot recognize A as a letter, so replace with another symbol
		}
	}
}
