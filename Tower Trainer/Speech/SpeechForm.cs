﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tower_Trainer.Properties;
using System.IO;
using System.Speech.Recognition.SrgsGrammar;
using System.Threading;
using System.Text.RegularExpressions;
using Tower_Trainer.Speech;

namespace Tower_Trainer
{
     public partial class SpeechForm : Form
     {

          // TODO: look into making this form generic
          IServiceSR<OutputRequestedEventArgs, TTSRequestedEventArgs> speechRecognitionService;
          IServiceTTS textToSpeechService;

          public SpeechForm()
          {
               InitializeComponent();
          }
          
          private void SpeechForm_Load(object sender, EventArgs e)
          {
               //locator = new ServiceLocator();

               textToSpeechService = new ServiceTTS();
               speechRecognitionService = new ServiceSR();
               //textToSpeechService = locator.getService<IServiceTTS>();
               //speechRecognitionService = locator.getService<IServiceSR<OutputRequestedEventArgs>> ();

                speechRecognitionService.OutputRequest += sr_OutputRequest;
                speechRecognitionService.TTSRequest += sr_TTSRequest;

          }

        private void sr_OutputRequest(object sender, OutputRequestedEventArgs e)
        {
            output_box.Text = e.outputString;
        }

        private void sr_TTSRequest(object sender, TTSRequestedEventArgs e)
        {
            textToSpeechService.CreateReadback(e.readback, e.aircraft);
        }

        private void start_btn_Click(object sender, EventArgs e)
          {
               speechRecognitionService.RecognizeAsync();
               start_btn.Enabled = false;
               end_btn.Enabled = true;
          }

          private void end_btn_Click(object sender, EventArgs e)
          {
               speechRecognitionService.RecognizeAsyncStop();
               start_btn.Enabled = true;
               end_btn.Enabled = false;
          }
     }
}
