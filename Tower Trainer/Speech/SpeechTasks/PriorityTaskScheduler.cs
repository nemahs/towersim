﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Tower_Trainer.Speech.SpeechTasks
{
    // TODO: Incomplete, do not use
    class PriorityTaskScheduler : TaskScheduler, IDisposable
    {
        private BlockingCollection<Task> tasksCollection;
        private readonly Thread mainThread = null;
        private bool currentThreadIsProcessingItems;

        public PriorityTaskScheduler()
        {
            tasksCollection = new BlockingCollection<Task>();

            mainThread = new Thread(new ThreadStart(Execute));
        }

        private void Execute()
        {
            foreach (var task in tasksCollection.GetConsumingEnumerable())
            {
                TryExecuteTask(task);
            }
        }

        protected override IEnumerable<Task> GetScheduledTasks()
        {
            return tasksCollection.ToArray();
        }

        protected override void QueueTask(Task task)
        {
            if (task != null)
                tasksCollection.Add(task);

            if (!mainThread.IsAlive) mainThread.Start();
        }

        protected override bool TryExecuteTaskInline(Task task, bool taskWasPreviouslyQueued)
        {
            return false;
        }

        public void Dispose()
        {
            tasksCollection.CompleteAdding();
            tasksCollection.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
