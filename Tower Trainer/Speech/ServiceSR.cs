﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Speech.Recognition;
using System.IO;
using System.Windows.Forms;

namespace Tower_Trainer.Speech
{

	// https://msdn.microsoft.com/en-us/library/hh362831(v=office.14).aspx

	// Need to download some voices: https://www.microsoft.com/en-us/download/details.aspx?id=27224
	// Then edit some registry keys: https://superuser.com/questions/590779/how-to-install-more-voices-to-windows-speech
	// to get FSX .dll C:\Program Files (x86)\Steam\steamapps\common\FSX\SDK\Core Utilities Kit\SimConnect SDK\lib\managed\Microsoft.FlightSimulator.SimConnect.dll
	// TODO: create an msi that includes the voices installer.

	// Runways: 9 Right/27 Left, 9 Left/27 Right, 

	enum Grammars
	{
		VFR_Departure,
		VFR_Departure_Initial,
		VFR_HoldShort_Cross,
        VFR_LUAW,
        VFR_Repeat,
		VFR_Takeoff,
		General,
		WhiteNoise
	};

    /// <summary>
    /// contains the grammar referenced by the speach recognition engine
    /// along some important context variables
    /// </summary> 
    /// <remarks>
    /// "GrammarReferenced" grammar that recognized incoming speech. 
    /// "OutputString" string that you want to display in a text box via interface.
    /// "Context" dictionary for all properties extracted from the grammar (e.g. callsign or taxiway).
    /// "IsValid" indicates if all propeerties related to grammar are set in the context'
    /// </remarks>
    class OutputRequestedEventArgs : EventArgs
    {
		public Grammars GrammarReferenced { get; set; }
		public string OutputString { get; set; }
		public Dictionary<string, string> Context { get; set; } 
			= new Dictionary<string, string>();

		public bool IsValid { get; set; } = true;
    }

    class ServiceSR: IServiceSR<OutputRequestedEventArgs>
    {

        SpeechRecognitionEngine sre;

        // EventHandler triggered whenever SR has output
        public event EventHandler<OutputRequestedEventArgs> OutputRequest;

        /// <summary>
        /// raises the event for when speach is recognized by a grammar
        /// </summary>
        /// <param name="e">The outputrequest arguments.</param>
        /// <remark>
        /// method raises an event for all suscribers to consume.
        /// consumption of event involves invoking the delegate (suscriber) method if any exists.
        /// </remark>
        protected virtual void OnOutputRequested(OutputRequestedEventArgs e)
        {            
            OutputRequest?.Invoke(this, e);
        }

        /// <summary>
        /// handles and categorizes all incoming speech based on the grammars found in xml/speech_recognition folder
        /// </summary>
        public ServiceSR()
        {
            sre = new SpeechRecognitionEngine();
            //sre.InitialSilenceTimeout = TimeSpan.FromSeconds(1.5);
            sre.BabbleTimeout = TimeSpan.FromSeconds(3);
            //sre.EndSilenceTimeout = TimeSpan.FromSeconds(1.5);
            //sre.EndSilenceTimeoutAmbiguous = TimeSpan.FromSeconds(2.5);

            loadGrammars();
            sre.SetInputToDefaultAudioDevice(); //sets the mic to the default device

            sre.SpeechRecognized += Sre_SpeechRecognized;
            //sre.SpeechHypothesized += Sre_SpeechHypothesized;
            sre.RecognizeCompleted += Sre_RecognizeCompleted;
            //sre.AudioStateChanged += Sre_AudioStateChanged;


        }
        
        
        private void loadGrammars()
        {
            // Find all the .grxml files in the speech recognition folder to add to sre
            string pathToSRE = Path.Combine(Environment.CurrentDirectory, "Speech", "xml", "speech_recognition");
            string[] grammarPaths = Directory.GetFiles(pathToSRE, "*.grxml");
            try
            {
                int iterator = 2;
                foreach (string grammarPath in grammarPaths)
                {
                    var grammar = new Grammar(grammarPath);
                    grammar.Name = Path.GetFileNameWithoutExtension(grammarPath);

                    // Make the general grammar have a lower priority of 1, with all the other custom grammars
                    // having larger priorities
                    grammar.Priority = grammar.Name == "General" ? 1 : iterator++;
                    sre.LoadGrammarAsync(grammar); //not gonna suspend current thread
                }

                // Load the regular dictation grammar to catch any background noise
                DictationGrammar dict = new DictationGrammar("grammar:dictation#pronunciation");
                dict.Name = "WhiteNoise";
                //sre.LoadGrammarAsync(dict);
            }

            catch (InvalidOperationException ex)
            {
                MessageBox.Show(ex.ToString(), "Error in loading grammar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error in loading grammar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Sre_AudioStateChanged(object sender, AudioStateChangedEventArgs e)
        {
            //AudioState newState = e.AudioState;
            //output_box.Text += newState.ToString() + " " + DateTime.Now;
        }

        private void Sre_RecognizeCompleted(object sender, RecognizeCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                //MessageBox.Show("Cancelled");
            }

            else if (e.Error != null)
            {
                MessageBox.Show(e.Error.ToString());
            }

            //output_box.Text += "completed\n";
            //throw new NotImplementedException();
        }

        private void Sre_SpeechHypothesized(object sender, SpeechHypothesizedEventArgs e)
        {
            //output_box.Text += "Hypothesized:\n";
            //output_box.Text += e.Result.Text;
            //output_box.Text += e.Result.Grammar.Name + "\n";
        }

        // We can only extract 4 tags from the grxml at a time due to its constraints
        private void Sre_SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
        {
            // Debugging to check for keys
            /*
            foreach (KeyValuePair<string, SemanticValue> pair in e.Result.Semantics)
            {
                MessageBox.Show(pair.Key.ToString());
            }
            */

            Grammars grammarName = (Grammars)Enum.Parse(typeof(Grammars), e.Result.Grammar.Name);
            OutputRequestedEventArgs eventArgs = new OutputRequestedEventArgs();
			eventArgs.GrammarReferenced = grammarName;

			if (grammarName == Grammars.General)
            {
                string output = e.Result.Semantics["phrase"].Value.ToString();
				string methods = e.Result.Semantics["method"].Value.ToString();

				try
				{
					if (e.Result.Semantics.ContainsKey("callsign"))
					{
						eventArgs.Context["callsign"] = e.Result.Semantics["callsign"].Value.ToString();
					}

					if (e.Result.Semantics.ContainsKey("runway"))
					{
						eventArgs.Context["runway"] = e.Result.Semantics["runway"].Value.ToString();
					}

					if (e.Result.Semantics.ContainsKey("taxiway"))
					{
						eventArgs.Context["taxiway"] = e.Result.Semantics["taxiway"].Value.ToString();
					}
				}

				catch (Exception) { }
	
				
                eventArgs.OutputString += e.Result.Semantics["method"].Value.ToString() + "\n\n";
                eventArgs.OutputString += e.Result.Semantics["phrase"].Value.ToString() + "\n\n";

				OnOutputRequested(eventArgs);
			}

            else if (grammarName == Grammars.VFR_Repeat)
            {
                string output = e.Result.Semantics["fullPhrase"].Value.ToString();
                string callsign = e.Result.Semantics["callsign"].Value.ToString();

                eventArgs.Context["callsign"] = callsign;
                eventArgs.GrammarReferenced = grammarName;

                eventArgs.OutputString += "Grammar referenced: " + e.Result.Grammar.Name;
                eventArgs.OutputString += "\nOutput: " + output;
                eventArgs.OutputString += "\nconfidence = " + e.Result.Confidence;

				OnOutputRequested(eventArgs);
			}

			// Process everything that's not white noise
			else if (grammarName != Grammars.WhiteNoise)
            {

                string output = e.Result.Semantics["fullPhrase"].Value.ToString();
				string readback = e.Result.Semantics["readbackPhrase"].Value.ToString();
                string callsign = e.Result.Semantics["callsign"].Value.ToString();

                eventArgs.Context["callsign"] = callsign;

                eventArgs.OutputString += "Grammar referenced: " + e.Result.Grammar.Name;
                eventArgs.OutputString += "\nOutput: " + output;
                eventArgs.OutputString += "\nconfidence = " + e.Result.Confidence;

                // Currently will match aircraft with 4 numbers and two letters, but that should be filtered
                // out by either the grxml or the lack of valid aircraft
                //string regexPattern = @"(?<fullPhrase>(?<callsign>N\d{1,4}[A-Z]{1,2}) runway (?<runway>([1-9]|[1-2]\d|3[0-6])[LRC]\b) taxi via(?<taxiway>(,?\s[A-Z]{1,2},?)+))";
                //Regex regex = new Regex(regexPattern, RegexOptions.ExplicitCapture);
                //MatchCollection matches = regex.Matches(output);

                //string callsign, runway, taxiway;
                //callsign = runway = taxiway = "";

                //foreach (Match match in matches)
                //{
                //    MessageBox.Show(match.Value);
                //    callsign = match.Groups["callsign"].Value;
                //    runway = match.Groups["runway"].Value;
                //    taxiway = match.Groups["taxiway"].Value;
                //}

				switch (grammarName)
				{
					case Grammars.VFR_Departure_Initial:
						{
							goto case Grammars.VFR_Departure;
							// carry through to VFR_Departure for all the code that is similar between them
						}

					case Grammars.VFR_Departure:
						{
							string runway = e.Result.Semantics["runway"].Value.ToString();
							string taxiway = e.Result.Semantics["taxiway"].Value.ToString();

							if (runway == string.Empty)
							{
								readback = $"Melbourne tower, {callsign}, you did not give a runway";
								eventArgs.IsValid = false;

							}

							else if (taxiway == string.Empty)
							{
								readback = $"Melbourne tower, {callsign}, you did not give a taxiway";
								eventArgs.IsValid = false;
							}

							else
							{
                                eventArgs.Context["runway"] = runway;
                                eventArgs.Context["taxiway"] = taxiway;
							}

							break;
						}

					case Grammars.VFR_HoldShort_Cross:
						{
							string runway = e.Result.Semantics["runway"].Value.ToString();
							eventArgs.IsValid = CheckRunway(ref readback, callsign, runway);

                            if (eventArgs.IsValid) eventArgs.Context["runway"] = runway;
							break;
						}

                    case Grammars.VFR_LUAW:
                        {
							// readback is empty unless there's a problem
							readback = string.Empty;
                            string runway = e.Result.Semantics["runway"].Value.ToString();
							eventArgs.IsValid = CheckRunway(ref readback, callsign, runway);

                            if (eventArgs.IsValid) eventArgs.Context["runway"] = runway;
                            break;
                        }

					case Grammars.VFR_Takeoff:
						{
							string runway = e.Result.Semantics["runway"].Value.ToString();
							eventArgs.IsValid = CheckRunway(ref readback, callsign, runway);

                            if (eventArgs.IsValid) eventArgs.Context["runway"] = runway;
                            break;
						}

				}

                eventArgs.Context["readback"] = readback;
				OnOutputRequested(eventArgs);
            }

        }

		private bool CheckRunway(ref string readback, string callsign, string runway)
		{
			if (runway == string.Empty)
			{
				readback = $"Melbourne tower, {callsign}, you did not give a runway";
                return false;
			}

            return true;
		}
        /// <summary>
        /// turns on speech recognition on method call 
        /// </summary>
        public void RecognizeAsync()
        {
            sre.RecognizeAsync(RecognizeMode.Multiple);
        }

        /// <summary>
        /// turns off speech recognition on method call
        /// </summary>
        public void RecognizeAsyncStop()
        {
            sre.RecognizeAsyncStop();
        }
    }
}
