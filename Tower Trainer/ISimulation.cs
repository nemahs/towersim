﻿using System.Collections.Generic;
using Tower_Trainer.Airport_Information;

namespace Tower_Trainer
{
    /// <summary>
    /// Provides an interface for simulations/mocks
    /// </summary>
    public interface ISimulation
    {
        List<Aircraft> CurrentAircraft { get; }
        Airport CurrentAirport { get; }

        void AddAircraft(Aircraft aircraft);
        void Start();
        void Stop();
    }
}