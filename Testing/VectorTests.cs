﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tower_Trainer.Tests
{
    [TestClass()]
    public class VectorTests
    {
        [TestMethod()]
        public void VectorTest()
        {
            var a = new Vector(0, 0);
            Assert.IsNotNull(a);
        }

        [TestMethod()]
        public void AddTest()
        {
            var a = new Vector(5, 5);
            var b = new Vector(10, 15);

            var result = a.Add(b);

            Assert.AreEqual(15, result.X);
            Assert.AreEqual(20, result.Y);
        }

        [TestMethod()]
        public void SubtractTest()
        {
            var a = new Vector(20, 20);
            var b = new Vector(10, 5);

            var result = a.Subtract(b);

            Assert.AreEqual(10, result.X);
            Assert.AreEqual(15, result.Y);
        }

        [TestClass()]
        public class NormalizeTests
        {
            [TestMethod()]
            public void NormalizeXTest()
            {
                var a = new Vector(30, 0);

                var results = a.Normalize();

                Assert.AreEqual(1, results.X);
                Assert.AreEqual(0, results.Y);
            }

            [TestMethod]
            public void NormalizeYTest()
            {
                var a = new Vector(0, 30);

                var results = a.Normalize();

                Assert.AreEqual(0, results.X);
                Assert.AreEqual(1, results.Y);
            }
        }

        [TestClass]
        public class HeadingTests
        {
            [TestMethod]
            public void ZeroDegreeTest()
            {
                var a = new Vector(0, 1);

                Assert.AreEqual(0, a.Heading);
            }

            [TestMethod]
            public void FourtyFiveDegreeTest()
            {
                var a = new Vector(1, 1);

                Assert.AreEqual(45, a.Heading);
            }

            [TestMethod]
            public void OneEightyDegreeTest()
            {
                var a = new Vector(0, -1);

                Assert.AreEqual(180, a.Heading);
            }

            [TestMethod]
            public void TwoSeventyDegreeTest()
            {
                var a = new Vector(-1, 0);

                Assert.AreEqual(270, a.Heading);
            }
        }
    }
}