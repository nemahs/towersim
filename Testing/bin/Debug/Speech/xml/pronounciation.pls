<?xml version="1.0" encoding="UTF-8"?>

<lexicon version="1.0" 
      xmlns="http://www.w3.org/2005/01/pronunciation-lexicon"
      alphabet="x-microsoft-ups" xml:lang="en-US">


  <lexeme>
    <grapheme> let </grapheme>
    <phoneme> Rick </phoneme>
  </lexeme>
  
  <lexeme>
    <grapheme> me </grapheme>
    <phoneme> mee </phoneme>
  </lexeme>

  <lexeme>
    <grapheme> smash </grapheme>
    <phoneme> smeeesh </phoneme>
  </lexeme>
  
</lexicon>