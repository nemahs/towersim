<?xml version="1.0" encoding="UTF-8"?>

  <lexicon version="1.0"
        xmlns="http://www.w3.org/2005/01/pronunciation-lexicon"
        alphabet="x-microsoft-ups" xml:lang="en-US">
    
    <lexeme>
      <grapheme> @ </grapheme>
      <phoneme> AE L F AX </phoneme>
    </lexeme>
    
    <lexeme>
      <grapheme> B </grapheme>
      <phoneme> B R AA V O </phoneme>
    </lexeme>
    
    <lexeme>
      <grapheme> C </grapheme>
      <phoneme> CH AA R L I </phoneme>
    </lexeme>

    <lexeme>
      <grapheme> K </grapheme>
      <phoneme>K I L O </phoneme>
    </lexeme>
    
    <lexeme>
      <grapheme> L </grapheme>
      <phoneme>L EH F T </phoneme>
    </lexeme>

    <lexeme>
      <grapheme> N </grapheme>
      <phoneme>N O V EH M B UR </phoneme>
    </lexeme>

</lexicon>