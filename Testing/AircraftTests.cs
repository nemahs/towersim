﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Moq;
using Tower_Trainer.P3D;
using System.Collections.Generic;
using Tower_Trainer.Airport_Information;

namespace Tower_Trainer.Tests
{
    public class AircraftTests
    {

        readonly Mock<ISimulation> sim = new Mock<ISimulation>();
        readonly Mock<IConnector> conn = new Mock<IConnector>();
        readonly Airport airport;

        public AircraftTests()
        {
            //Setup Mocks
            sim.Setup(x => x.CurrentAircraft).Returns(new List<Aircraft>());

            airport = new Airport(0, 0, new Dictionary<string, RunwayNode>(),
                new Dictionary<int, TaxiNode>(), new List<Link>(),
                new Dictionary<int, ParkingNode>(), new BiDictionary<int, string>());

        }

        protected virtual Aircraft GenericAircraft(double lat = 0, double lon = 0)
        {
            var a = new Aircraft(0, "N123AB", sim.Object, conn.Object, airport, lat, lon);
            var setup = new PrivateObject(a);
            //Do some basic setup to save code later
            setup.SetField("fullRoute", GenericRoute());
            setup.SetField("holdNode", 0);
            a.DestinationRunway = "9";

            return a;
        }

        protected static Node[] GenericRoute()
        {
            return new Node[] {
                new TaxiNode(0,0),
                new TaxiNode(1,0)
            };
        }

        [TestClass]
        public class AircraftGenerationTests: AircraftTests
        {
            [TestMethod()]
            public void AircraftTest()
            {
                var a = GenericAircraft();
                Assert.IsNotNull(a);
            }
        }

        [TestClass]
        public class UpdateTests : AircraftTests
        {
            [TestMethod()]
            public void UpdateTest()
            {
                var a = GenericAircraft();
                a.Update(new P3D.Connector.AircraftInfo
                {
                    lat = 1.5,
                    lon = 2.3,
                    currentWaypoint = 15,
                    groundVelocity = 30,
                    heading = Math.PI
                });

                Assert.AreEqual(1.5, a.Latitude, .05);
                Assert.AreEqual(2.3, a.Longitude, .05);
                Assert.AreEqual(15, a.CurrentWaypoint);
                Assert.AreEqual(30, a.GroundVelocity);
                Assert.AreEqual(180, a.Heading, .1);
            }
        }

        public class InFrontOfTests : AircraftTests
        {
            [TestMethod()]
            public void InFrontOfTest()
            {
                var a = GenericAircraft(0, 0);
                a.Heading = 90;
                var b = GenericAircraft(0, 1);

                Assert.IsTrue(b.InFrontOf(a));
            }

            [TestMethod]
            public void AircraftBehindTest()
            {
                var a = GenericAircraft(0, 0);
                var b = GenericAircraft(0, -1);

                Assert.IsFalse(b.InFrontOf(a));
            }
        }

        [TestClass]
        public class DepartingTests : AircraftTests
        {
            [TestMethod()]
            public void DepartTest()
            {
                var a = GenericAircraft();
                var setup = new PrivateObject(a);
                setup.SetProperty("Status", Aircraft.AircraftStatus.ShortOfRunway);

                var result = a.Depart("9");

                Assert.IsTrue(result);
                conn.Verify(x => x.MoveAircraft(a, It.IsAny<Node[]>()));
                Assert.AreEqual(Aircraft.AircraftStatus.Departing, a.Status);
            }

            [TestMethod]
            public void AircraftDoesntDepartWhileTaxiingTest()
            {
                var a = GenericAircraft();
                var setup = new PrivateObject(a);
                setup.SetProperty("Status", Aircraft.AircraftStatus.Taxiing);

                var result = a.Depart("9");

                Assert.IsFalse(result);
                conn.Verify(x => x.MoveAircraft(a, It.IsAny<Node[]>()), Times.Never());
                Assert.AreEqual(Aircraft.AircraftStatus.Taxiing, a.Status);

            }

            [TestMethod]
            public void AircraftDoesntDepartWhenGivenTheWrongRunway()
            {
                var a = GenericAircraft();
                var setup = new PrivateObject(a);
                setup.SetProperty("Status", Aircraft.AircraftStatus.ShortOfRunway);

                var result = a.Depart("5");

                Assert.IsFalse(result);
                conn.Verify(x => x.MoveAircraft(a, It.IsAny<Node[]>()), Times.Never());
                Assert.AreEqual(Aircraft.AircraftStatus.ShortOfRunway, a.Status);
            }
        }

        [TestClass]
        public class StopTests : AircraftTests
        {
            [TestMethod()]
            public void StopTest()
            {
                var a = GenericAircraft();
                a.Stop();

                conn.Verify(x => x.Stop(a));
            }
        }

        [TestClass]
        public class GoTests : AircraftTests
        {
            [TestMethod()]
            public void GoTest()
            {
                var a = GenericAircraft();

                a.Go();

                conn.Verify(x => x.SetSpeed(a, 20));
            }
        }

        [TestClass]
        public class ContinueTaxiTests : AircraftTests
        {
            readonly TaxiNode endnode = new TaxiNode(0, 1);

            protected override Aircraft GenericAircraft(double lat = 0, double lon = 0)
            {
                var a = base.GenericAircraft(lat, lon);
                a.CurrentWaypoint = -1;
                var setup = new PrivateObject(a);
                setup.SetProperty("ShortOf", "9");
                setup.SetField("fullRoute", new Node[] {
                    new TaxiNode(0,0),
                    endnode
                });

                return a;
            }

            [TestMethod()]
            public void ContinueTaxiTest()
            {
                var a = GenericAircraft();
                var setup = new PrivateObject(a);
                setup.SetProperty("Status", Aircraft.AircraftStatus.HoldShort);

                var result = a.ContinueTaxi("9");

                Assert.IsTrue(result);
                conn.Verify(x => x.MoveAircraft(a, new Node[] { endnode }));
                Assert.AreEqual(Aircraft.AircraftStatus.Taxiing, a.Status);
            }

            [TestMethod]
            public void ContinueTaxiDoesNothingWhileTaxiingTest()
            {
                var a = GenericAircraft();
                var setup = new PrivateObject(a);
                setup.SetProperty("Status", Aircraft.AircraftStatus.Taxiing);

                var result = a.ContinueTaxi("9");

                Assert.IsFalse(result);
                conn.Verify(x => x.MoveAircraft(a, It.IsAny<Node[]>()), Times.Never());
                Assert.AreEqual(Aircraft.AircraftStatus.Taxiing, a.Status);
            }

            [TestMethod]
            public void ContinueTaxiDoesNothingWhenGivenTheWrongRunwayTest()
            {
                var a = GenericAircraft();
                var setup = new PrivateObject(a);
                setup.SetProperty("Status", Aircraft.AircraftStatus.HoldShort);

                var result = a.ContinueTaxi("5");

                Assert.IsFalse(result);
                conn.Verify(x => x.MoveAircraft(a, It.IsAny<Node[]>()), Times.Never());
                Assert.AreEqual(Aircraft.AircraftStatus.HoldShort, a.Status);
            }
        }

        [TestClass]
        public class LineUpAndWaitTests : AircraftTests
        {
            readonly Node endnode = new TaxiNode(1, 0);

            protected override Aircraft GenericAircraft(double lat = 0, double lon = 0)
            {
                var a = base.GenericAircraft(lat, lon);
                var setup = new PrivateObject(a);
                a.CurrentWaypoint = -1;
                setup.SetField("fullRoute", new Node[] {
                    new TaxiNode(0,0),
                    endnode
                });

                return a;
            }

            [TestMethod()]
            public void LineUpAndWaitTest()
            {
                var a = GenericAircraft();
                var setup = new PrivateObject(a);
                setup.SetProperty("Status", Aircraft.AircraftStatus.ShortOfRunway);

                var result = a.LineUpAndWait("9");

                Assert.IsTrue(result);
                conn.Verify(x => x.MoveAircraft(a, new Node[] { endnode }));
                Assert.AreEqual(Aircraft.AircraftStatus.LUAW, a.Status);
            }
        }
    }
}