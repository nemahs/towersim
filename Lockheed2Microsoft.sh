#!/bin/bash
grep -rl "using Microsoft.FlightSimulator.SimConnect" ./Tower\ Trainer | sed -e "s@\ @\\\ @g" | xargs sed -i "/Microsoft/s|^/*||g"
grep -rl "using LockheedMartin.Prepar3D.SimConnect" ./Tower\ Trainer | sed -e "s|\ |\\\ |g" | xargs sed -i "/Lockheed/s@^@//@g" 