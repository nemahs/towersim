# Overview
vTower is a program designed to help teach Air Traffic Control students the phraseology and techniques used in the Air Traffic Control Tower (ATCT) environment.
The program uses Lockheed Martin's Prepar3D (P3D) flight simulation software to create traffic scenarios that the user can control using speech.

Website: https://nemahs.gitlab.io/towersim

# Common Terms and Acronyms

- ATC = Air Traffic Control
- ATCT = Air Traffic Control Tower. Also known as *Local*
- TRACON = Terminal RAdar CONtrol. Also known as *Approach*
- FSX = Microsoft Flight Simulator X, the flight simulator Prepar3D is based off of
- P3D = Lockheed Martin Prepar3D
- 7710.65 = FAA publication detailing how controllers should conduct their job. Used as a reference for controller phraseology. Also known as *the 7710* or *the .65* or *the ATC bible*

# Development Requirements
- Visual Studio 2017
- Moq (Install using the command `Install-Package -Project Testing Moq` in Nu-Get)
- SharpOVR (Install using the command `Install-Package SharpOVR` in Nu-Get)
- Microsoft Visual Studio 2017 Installer Projects (Install through the extension manager in Visual Studio)
- P3D/FSX (FSX is acceptable if that is what the developer has, but the API is more limited that what P3D offers)
- Simconnect
  * **A note about installing Simconnect:**
  * If you have P3D, simconnect can be installed by installing the P3D SDK located here: https://www.prepar3d.com/support/sdk/
  The .dll that the Visual Studio project uses is located at Program Files (x86)/Lockhhed Martin/Prepar3D v# SDK ###/Utilities/SimConnect/lib/managed where ### represent version numbers 

  * If you have FSX you will have to install Simconnect from <game dir>/SDK/Core Utilities Kit/SimConnect SDK
  The .msi is located under LegacyInterfaces, and the .dll for Visual Studio is located in lib/managed

# Testing
    
For the most part, testing should be done using unit tests. Unfortunately there aren't that many unit tests at the moment, so future teams should pin down more code with unit tests.

Simconnect code needs either FSX or P3D to run to visually verify that the correct command was executed. Because of this simconnect code should remain in the Connector class and
Connector should be mocked during unit testing (like in AircraftTests.cs)

# Building the Installer

The installer can be built from Visual Studio. The project will build vTower and package it and its dependencies into an msi file placed in Installer's bin directory.

**NOTE:**
The installer currently does not install everything needed. Running vTower after installing using the installer leads to the error message: `simconnect.dll could not be loaded`

Installing Visual Studio with C# development tools fixes the problem, but this is obviously not ideal. 

# Editing/Building the Website

The code for the website exists in the site directory of this repository. It is written in Jekyll. `.gitlab-ci.yaml` contains instructions for Gitlab to build and deploy the
website anytime a commit is pushed to master.